Title: Stax is going open source
Date: 2021-03-03
Category: News
Tags: news, open source
Slug: stax-open-source
Author: Félix David, Tristan Le Granché
Lang: fr

Après un peu plus d'un an de développement interne chez SuperProd Studio, Stax déploie ses ailes et rejoint le monde de l'OpenSource !

## Génial ! Mais Stax, c'est quoi ?

Un ensemble d'outils de **review** basé sur le [*VSE*](https://docs.blender.org/manual/en/latest/video_editing/sequencer/index.html) de Blender.  

Après avoir testé de nombreux outils de review, sans en trouver un seul qui corresponde réellement à nos besoins, nous avons décidé de créer notre propre ensemble d'outils : Stax.

## Principales features

- Session de review
    - Le *VSE* (module de montage) de Blender permet aux utilisateurs de faire de la review sur des timelines complètes. Plus besoin de faire une review media par media.
    - Chaque media est dans son contexte de montage, avec ses versions et étapes précédentes.
    - Et vous pouvez, de manière aussi simple que cliquer sur le bouton play, regarder tout votre montage avec vos notes et celles de vos collègues.

- Comparaison inter-média, pour comparer deux versions par exemple
    - Transparence
    - Mode de fusion (Multiply, Difference...)
    - Wipe
    - Side-by-side

- Ajout/édition de notes
    - Grâce au [*Grease Pencil*](https://docs.blender.org/manual/en/latest/grease_pencil/index.html) de Blender, vous pouvez ajouter des notes visuelles facilement, les modifier, les dupliquer etc.
    - Stax intègre aussi un module de notes textes pour des explications complexes.
    - Et bien sûr les notes peuvent avoir un frame range.

- Publication des notes
    - Si vous travaillez avec un Production Tracker (type Kitsu, Shotgun...), vous pouvez y connecter Stax, pour publier vos reviews et les rendre disponibles pour toutes vos équipes.

Et bien d'autres fonctionnalités !

## L'OpenSourcing de Stax

Tout d'abord car nous avons la volonté de partage.  
Ensuite car Stax a fait l'objet d'un intérêt certain auprès de plusieurs studios, et que s'il peut satisfaire la totalité des besoins de *review*, une mise en commun est indispensable à son évolution.

## Nous ne sommes pas seuls

Stax est porté par SuperProd Studio.  
Ubisoft, The Yard et Malil'Art participent à son développement.

## Le futur de Stax ?

- Ajout de features en fonction des nouveaux besoins
- Plus de flexibilité pour faciliter son intégration aux différents *workflows*
- Séparation de certaines features en modules indépendants pouvant être utilisés en dehors d'un cadre de review

## Vous souhaitez rejoindre l'aventure ?

- Rejoignez notre [Discord](https://discord.gg/8ZNx7Vz) 
- Découvrez la [Documentation](https://superprod.gitlab.io/stax_suite/stax/docs/) (en anglais)
- Lisez le document de [Contributing](https://gitlab.com/superprod/stax_suite/stax/-/blob/master/CONTRIBUTING.md)
- Fouillez le [GitLab](https://gitlab.com/superprod/stax_suite/stax/)

Actuellement Stax est fourni sous la forme d'un [Application Template](https://docs.blender.org/manual/en/latest/advanced/app_templates.html).

## En vidéo, c'est toujours mieux

Pour avoir une idée plus précise de l'état actuel de Stax et des besoins auxquels il répond, vous pouvez visionner notre intervention au [RADI 2021](https://youtu.be/MY2fEUNfJKE?t=1010).

## Pourquoi le nom Stax ?

En référence à la notion d'empilement des médias (*stacks*), une des spécificités de Stax, et en hommage au label de musique [Stax Records](https://staxrecords.com/).

## Dites m'en plus !

- Stax utilise [OpenTimelineIO](https://opentimelineio.readthedocs.io/en/latest/) pour charger les montages
- Nous avons créé [OpenReviewIO](https://pypi.org/project/openreviewio/), pour faciliter les échanges d'informations de *review* entre les logiciels
- Le tout en Python 3