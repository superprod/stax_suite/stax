Title: Stax 3.0, new version, new recipe!
Date: 2021-12-21
Category: News
Tags: news, Stax 3
Slug: stax-3.0
Author: Félix David
Lang: en

Since several months, Stax development is building a new, more flexible and faster core design.
This refactor ensures a reliable progress during the upcoming months.  
*If you're wondering what is Stax about, you may enjoy reading [the article about the open sourcing](https://superprod.gitlab.io/stax_suite/stax/stax-open-source.html).*

<script src="http://vjs.zencdn.net/4.0/video.js"></script>

<video id="pelican-installation" class="video-js vjs-default-skin" controls
preload="auto" width="683"
data-setup="{}">
<source src="videos/Stax_SH_Workflow1.webm" type='video/webm'>
</video>

## Tell me more!

Using the *2.x* versions, the editing timeline building was requiring to import an editing file strictly structured. Although this system used [OpenTimelineIO](https://opentimelineio.readthedocs.io/en/latest/), it was very restrictive.
With *3.x* series, Stax doesn't consider the way the timeline is built, reviewing it is easy without having to make any conformation.
Freedom is absolute and leaves space to focus on the smartest way to structure the editing for an efficient review.

## OpenTimelineIO cannot be used anymore?

That's the opposite! Import/export editing management with Blender has been moved to a dedicated add-on : [VSE IO](https://gitlab.com/superprod/stax_suite/vse_io).
It can be used freely with or without Stax.

## What about speed?

The removal of the internal architecture, which was constraining the editing's structure in *2.x* versions, implied a slow-down for all processes. Today, Stax relies only on VSE's timeline!
Furthermore, optimisations regarding the existing features have been made.
This is fast! Even with huge editings!

## No any new feature then?

A brand new review workflow has been designed to guarantee Stax usage independantly from any pipeline.
It makes easy to publish and link reviews from a local drive or a server without requiring any production tracker (watch video at the beginning of the article).

## New impulse

Since today Stax was supported by SuperProd Studio, mainly developed by Félix David. It happens he quits SPS to become head of pipeline at Normaal Animation. Which means Stax is going to be used in another studio as the main review tool.

More studios are seriously considering Stax for their artists, attracted by the numerous benefits, and new collaborations will surely emerge for specific areas of the project.

[CGWire][https://www.cg-wire.com/] also started to work on a direct link between Stax and their production tracker, [Kitsu](https://www.cg-wire.com/en/kitsu.html), stay tuned!

### Would you get on board?

- Join our [Discord Channel](https://discord.gg/8ZNx7Vz) 
- Explore the [Documentation](https://superprod.gitlab.io/stax_suite/stax/docs/)
- Read the [Contributing](https://gitlab.com/superprod/stax_suite/stax/-/blob/master/CONTRIBUTING.md) Doc
- Rummage our [GitLab](https://gitlab.com/superprod/stax_suite/stax/)

Currently Stax is provided as an [Application Template](https://docs.blender.org/manual/en/latest/advanced/app_templates.html).

### A video for thousand words

To picture Stax's current state and the needs it fits, you can watch our presentation at [RADI 2021](https://youtu.be/MY2fEUNfJKE?t=1010), in french.