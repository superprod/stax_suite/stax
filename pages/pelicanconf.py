#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

THEME = "themes/striped"
AUTHOR = "Félix David"
SITENAME = "Stax"
SITELOGO = "images/stax_logo_dark_BG.png"
SITEURL = "https://superprod.gitlab.io/stax"
SITESUBTITLE = "OpenReviewIO for Blender"
SITEDESCRIPTION = "Stax is the best open source reviewing tool!"

PATH = "content"
OUTPUT_PATH = "public"

TIMEZONE = "Europe/Paris"
LOCALE = ("en_US", "fr_FR")  # On Unix/Linux

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ("Documentation", "docs"),
    ("Repository", "https://gitlab.com/superprod/stax"),
    ("Blender", "https://blender.org/"),
)

# Social widget
SOCIAL = (
    ("Chat with us", "https://discord.gg/8ZNx7Vz"),
    ("Contribute", "https://gitlab.com/superprod/stax"),
)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

STATIC_PATHS = ["images", "extra", "videos"]
EXTRA_PATH_METADATA = {"extra/favicon.ico": {"path": "favicon.ico"}}

# Custom jinja filters
from textwrap import shorten

JINJA_FILTERS = {"shorten": shorten}
