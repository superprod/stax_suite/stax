==========================
Extend Stax
==========================

.. warning:: 

    Extending Stax is still under development and the design is not properly defined. 
    By using this technics you are aware you enter a dark area and the Stax team doesn't ensures any compatibility with upcoming versions.


It's possible to `Override`_ the whole Stax's UI and operators.

.. important::

   Stax is based on Blender and meant to stick as much as possible to its concepts. 
   Then, knowing `Blender Python API`_ is helpful, and learning Stax's API is learning few of Blender's.

.. _`Blender Python API`: https://docs.blender.org/api/current/

.. _`Override`: `Override Classes`_

.. include:: extend/override_classes.rst