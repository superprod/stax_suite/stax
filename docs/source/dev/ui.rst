=====================
UI
=====================

Configuration
============================
``ui_configuration.py``

.. automodule:: stax.ui.ui_configuration
   :members:
   :undoc-members:
   :show-inheritance:

Drawing
======================
``ui_drawing.py``

.. automodule:: stax.ui.ui_drawing
   :members:
   :undoc-members:
   :show-inheritance:

Export
======================
``ui_export.py``

.. automodule:: stax.ui.ui_export
   :members:
   :undoc-members:
   :show-inheritance:

Sequencer
=========================
``ui_sequencer.py``

.. automodule:: stax.ui.ui_sequencer
   :members:
   :undoc-members:
   :show-inheritance:

Text Editor
==========================
``ui_texteditor.py``

.. automodule:: stax.ui.ui_texteditor
   :members:
   :undoc-members:
   :show-inheritance:

Time
====================
``ui_time.py``

.. automodule:: stax.ui.ui_time
   :members:
   :undoc-members:
   :show-inheritance: