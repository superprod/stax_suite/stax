=====================
Core Development
=====================

Design
============
Stax relies as much as possible on Blender's architecture and functionnalities. The main idea is to code as less as possible and only what Blender doesn't already provide. Regarding to this philosophy, Stax requires to understand how Blender works and how to hack it wisely.

Media Sequence
--------------
As a review is media based (related to an unique media), it is very important to always work with the media sequence, the sequence actually displaying the image/video in the preview.
To avoid mistakes from meta sequences nesting, every operation and any data must be considered using the media sequence.

Stax has a function for that: 

.. autofunction:: stax.utils.utils_timeline.get_media_sequence


Annotation frame management
---------------------------
An annotation is a review element related to a specific frame and duration of a media. When an offset is set to a media, because of handles for example, then the media and its sequence in the timeline have different timing values.

Stax deals annotation's frames by using absolute frame (to the timeline) or relative frame (to the media).

* To get the relative/media frame value: ``sequence.frame_start`` which is in Blender the raw value of the sequence in the sequencer, without any offset.
* To get the absolute/timeline frame value: ``sequence.frame_final_start`` which is in Blender the actual value of the sequence in the sequencer, any offset applied.

Most of the time, the ``media_sequence`` is used for ``sequence``.

Python API
==========

.. toctree::
   :maxdepth: 4

   api