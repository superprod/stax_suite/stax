===================
Reviewers & Artists
===================

Stax allows you to:

-  **View** medias and related reviews, text comments and drawings annotations.
-  **Compare** medias with previous versions. 
-  **Create** visual and text notes on medias.
-  **Publish** your review notes.

.. important::

   In Stax you always work and display **media** files.

Workspaces
==========
Stax has different workspaces described in the following chapters. They are accessible from the ``Workspace`` menu.

.. figure:: /_static/images/Workspace_Menu.png
    :alt: Workspace menu

    Workspace Menu

.. toctree::
   :caption: Workspaces

   reviewing_main_workspace
   reviewing_advanced_drawing
   export_timeline
   configuration

What if I find a bug
====================

**No way it happens !**

But if that happens, first follow these steps :

-  Delete your Stax config file ``cfg.json`` in
   ``C:\Users\UserName\AppData\Roaming\Stax``

If there’s still a problem, ask for help on the discord_ channel.

.. _discord: https://discord.gg/8ZNx7Vz
