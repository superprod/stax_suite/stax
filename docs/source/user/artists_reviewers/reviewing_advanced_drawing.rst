===========================================
Reviewing in the Advanced Drawing workspace
===========================================

This workspace provides much more drawing tools than the *Main* workspace.

.. important:: 
    This mode only works on one media at a time.  
    You can't use it to review multiple media at once.


Workflow
========
You enter it either by the *Preview* ``Toggle Advanced Drawing`` button on the left toolbar or 
by clicking on ``Advanced Drawing`` in the workspace selection topbar. 

.. figure:: ../images/AdvancedDrawingAccess.png
    :alt: Accesses to Advanced Drawing

    Accesses to Advanced Drawing

The currently displayed media will be selected to be reviewed. 

When you're done drawing, click on ``Complete Drawing`` to go back to the **Main** workspace.

.. video:: ../images/Drawing_Workflow.mp4
    :autoplay: yes
    :loop: yes
    :controls: no

|

Modes & Tools
=============
The Advanced Drawing workspace provides two modes: `Draw`_ and `Edit`_. 
You can change from one mode to another by clicking on the button or by pressing ``TAB``.

.. figure:: ../images/Tool_Modes.png
   :alt: Tool Mode Selection

   Tool Mode Selection

Each mode has a complete set of tools you can access by clicking on them on the left toolbar.

The active tool settings are accessible from the tool header below.

.. important::
    All tools aren't documented, you can refer to `Blender drawing tools`_.

.. _Blender drawing tools: https://docs.blender.org/manual/en/latest/grease_pencil/modes/draw/tools.html


Draw
----
Draw mode allows to draw using classic tools.

Draw Pencil
^^^^^^^^^^^

.. figure:: ../images/Tool_Draw.png
   :alt: Tool Draw Selected

   Tool Draw Selected


As for the *Main* workspace, **Left click** to draw, but the **Right click** doesn't erase, you have to select the Erase_ tool.

.. tip::
    The drawing tablet is supported.




**Stroke and Fill**
    - Radius
    - Strength
    - Opacity

.. video:: ../images/Tool_Draw_Stroke-Fill.mp4
    :autoplay: yes
    :loop: yes
    :controls: no


|

**Color palette**

    - Choose a color among a set 
    - Pick a custom color
    - Two stored colors

.. figure:: ../images/ColorPalette.png
   :alt: ColorPalette
   :scale: 50 %
   :align: center

   ColorPalette

.. video:: ../images/Tool_Draw_ColorPalette.mp4
    :autoplay: yes
    :loop: yes
    :controls: no


|

Erase
^^^^^^

.. figure:: ../images/Tool_Erase.png
   :alt: Tool Erase Selected

   Tool Erase Selected

- Dissolve
- Point
- Stroke

.. video:: ../images/Tool_Erase.mp4
    :autoplay: yes
    :loop: yes
    :controls: no

|

Shapes
^^^^^^
.. figure:: ../images/Tool_Shapes.png
   :alt: Tool Rectangle Shape Selected
   :scale: 50 %

   Tool Rectangle Shape Selected

#. Draw the shape
#. Press ``Enter``

.. video:: ../images/Shape_Rectangle.mp4
    :autoplay: yes
    :loop: yes
    :controls: no

    Rectangle Shape

|

Edit
----
Edit mode allows to edit drawn points. Using the shortcuts or the gizmos, you can cancel the modification with ``Esc`` or ``Right Mouse``.


Select
^^^^^^
Select points using ``Left Mouse`` button dragging. Default tool.

.. figure:: ../images/Tool_Select.png
   :alt: Tool Select Selected

   Tool Select Selected


Move
^^^^
``G``

Move selected points using the gizmo.

.. figure:: ../images/Tool_Move.png
   :alt: Tool Move Selected

   Tool Move Selected


Rotate
^^^^^^
``R``

Rotate selected points using the gizmo.

.. figure:: ../images/Tool_Rotate.png
   :alt: Tool Rotate Selected

   Tool Rotate Selected


Scale
^^^^^^
``S``

Scale selected points using the gizmo.

.. figure:: ../images/Tool_Scale.png
   :alt: Tool Scale Selected

   Tool Scale Selected


Transform
^^^^^^^^^^

Gizmo combining `Move`_, `Rotate`_ and `Scale`_.

.. figure:: ../images/Tool_Transform.png
   :alt: Tool Transform Selected

   Tool Transform Selected


Extrude
^^^^^^^^^^
``E``

Duplicate points while keeping the new points connected with the original ones.

.. figure:: ../images/Tool_Extrude.png
   :alt: Tool Extrude Selected

   Tool Extrude Selected


Radius
^^^^^^^^^^
``Alt + S``

Expand or contract the thickness radius of the selected points.

.. figure:: ../images/Tool_Radius.png
   :alt: Tool Radius Selected

   Tool Radius Selected


Bend
^^^^^^^^^^
``Shift + W``

Bend selected points.


.. figure:: ../images/Tool_Bend.png
   :alt: Tool Bend Selected

   Tool Bend Selected


Shear
^^^^^^^^^^
``Shift + Ctrl + Alt + S``

Shear selected points along the horizontal or vertical screen axis.

.. figure:: ../images/Tool_Shear.png
   :alt: Tool Shear Selected

   Tool Shear Selected



.. include:: edit_comment.rst


Display
=======
* Toggle drawings
* :ref:`emphasize drawings` (change the whiteness of the media)
* Flip X & Y
* Onion Skinning

.. video:: ../images/Drawing_Display.mp4
    :autoplay: yes
    :loop: yes
    :controls: no


Controls
========

.. tip::
    Shortcuts are the same as for Blender, then if you know what you're doing, the force is with you.

Keyframes
---------
- Go to next/previous keyframe: ``Arrow Up/Down``
- Select: ``Left click``
- Box select: ``Drag Left Click`` or ``B + Left Click``
- Move selected: ``Drag Left Click`` or ``G + Left Click``
- Delete Keyframe: ``Del`` or ``X``
- Duplicate Keyframe: ``Shift + D```
- Select all: ``A``
- Deselect all: ``AA`` or ``Alt + A``

.. video:: ../images/Drawing_Keyframes.mp4
    :autoplay: yes
    :loop: yes
    :controls: no





