==============
Configure Stax
==============


You can access Stax’s configuration using the **User Configuration** tab.
Your settings changes are saved automatically.

------------------
User configuration
------------------

Colors
======
* :ref:`Image annotation marker <pending-annotations-drawings>`:

.. figure:: /_static/images/ImageAnnotations_Marker_ColorConfiguration.png
        :alt: Image annotation marker color configuration
            
        Image annotation marker color configuration


.. _set-external-image-editor:

Set external image editor
=========================

You have to target the image editor executable via a file browser.

#.  Click on the **folder** icon
#.  Go search for your editor executable file
#.  Click **Accept**
#.  Click **Save User Configuration**
#.  That’s it

--------------------
System configuration
--------------------
As Stax is built upon Blender, the system configuration is a Blender configuration.

Video Cache Memory
==================

.. figure:: /_static/images/Configuration_VideoMemory.png
            :alt: Video Cache Memory
            
            Video Cache Memory

The `Video Sequencer <https://docs.blender.org/manual/en/latest/editors/preferences/system.html#video-sequencer>`_ has several features to increase the performances:

* ``Memory Cache Limit``: RAM cache memory size, increase it the amount you like knowing that more is better but you must preserve some memory space for other applications if you use them simultaneously.
Considering you're using Blender as the main software, you can leave 1/5 of memory for others. As the field suports maths, you can type: ``{total_ram} * 4 / 5``.
For example, if your RAM amount allows it, you can set the value of ``20000`` to raise the memory video limit up to 20Go.
* ``Disk Cache``: Write cache frames to disk. Select a disk with a fast rate else it may slow down the scrubbing.

