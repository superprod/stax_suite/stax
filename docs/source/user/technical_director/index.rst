===================
Technical Director
===================
To set a user's session up, you can start Stax with the editing you want and link its related reviews.

.. highlight:: python

-------
Editing
-------
The method to set a session up with an exising editing is absolutely free. Here are a few usual ways to proceed:

From an editing file
====================
**This is currently the recommended method**

This method requires the `VSE IO <https://gitlab.com/superprod/stax_suite/vse-io>`_ Blender add-on to be installed which uses `OpenTimelineIO <https://opentimelineio.readthedocs.io/en/latest/>`_. 
You can check the compatible `editing files <https://gitlab.com/superprod/stax_suite/vse-io#supported-formats>`_.

.. code-block:: python
   
   import bpy

   bpy.ops.sequencer.import_editing(
      filepath="/path/test.otio",
   )


From a Blender file
===================
You can open a previous Stax session or a ``.blend`` file after the ``stax`` application template has been started.

.. code-block:: python
   
   import bpy

   # Start Stax
   bpy.ops.wm.read_homefile(app_template="stax")

   # Open your editing
   bpy.ops.wm.open_mainfile(
      filepath="/path/my_editing.blend",
   )

.. warning:: 

   Because of some present :ref:`limitations <known limitations>`, this method has prerequisites:

   * Your editing must be in a scene called `'Scene'`
   * This scene must be the active one when opening the file


Build my own editing
====================
If you need to dynamically set the timeline up when the session starts, there are two ways:

* Operators: `Add movie strip <https://docs.blender.org/api/current/bpy.ops.sequencer.html#bpy.ops.sequencer.movie_strip_add>`_

.. code-block:: python
   
   import bpy

   bpy.ops.sequencer.movie_strip_add(
      filepath="/path/to/my_movie.mkv",
   )

* Python API: `New movie <https://docs.blender.org/api/current/bpy.types.SequencesTopLevel.html#bpy.types.SequencesTopLevel.new_movie>`_

.. code-block:: python
   
   import bpy

   bpy.context.scene.sequence_editor.sequences.new_movie(
      name="Name",
      filepath="/path/to/my_movie.mkv",
      channel=1,
      frame_start=0,
   )

-------
Reviews
-------
Reviews are linked to their media sequences according to the ``orio_review_path`` key in `Custom Properties <https://docs.blender.org/manual/en/latest/files/data_blocks.html#files-data-blocks-custom-properties>`_.
Once these properties are set, the ``bpy.ops.sequencer.update_reviews()`` operator will build them into the timeline.

To set the property to a media sequence:

.. code-block:: python
   
   import bpy

   # Link review
   media_sequence = bpy.context.scene.sequence_editor.sequences[0]
   media_sequence["orio_review_path"] = "/path/to/my_movie.ext.orio"

   # Build reviews
   bpy.ops.sequencer.update_reviews()

You can also use the :ref:`operators <reviews>` provided by Stax.

------
Launch
------
The python script must be executed through Blender. 
You can write your script into a ``.py`` file and launch Blender with this file using a command.

.. _`text Editor`: https://docs.blender.org/manual/en/latest/editors/text_editor.html

.. _`command line`:

Command Line
============
.. highlight:: bash

Raw command:

.. code-block:: bash

   /path/to/blender --app-template stax --python-expr "import bpy;bpy.ops.sequencer.import_editing(filepath='/path/test.otio')"


Target a script containing the python code:

.. code-block:: bash

   /path/to/blender --app-template stax --python path/to/start_stax.py

.. include:: /features/timeline.rst
   :start-after: bpy.ops.sequencer.import_editing(filename="/path/to/timeline.otio")
   :end-before: Trouble shooting

--------------------
What if I find a bug
--------------------

**No way it happens !**

But if that happens, first follow these steps :

-  Delete your Stax config file ``cfg.json`` in
   ``C:\Users\UserName\AppData\Roaming\Stax``

If there’s still a problem, ask for help on the discord_ ``#dev`` channel.

.. _discord: https://discord.gg/fceFNFH
