
==========================
Timeline
==========================

.. _`import editing`:

--------------
Import editing
--------------

Loading an editing in Stax relies entierly on Blender's features, from Drag&Drop to scripted construction.

To load an editing file such as ``xml``, ``edl``... you can use the `VSE IO`_ add-on from Stax Suite.
Once this add-on installed it'll be available in the ``Session`` menu.

How to
======
1. Click on ``Import Editing``.
2. Select the editing file using the file browser.

.. note:: Check the `VSE IO Documentation <https://gitlab.com/superprod/stax_suite/vse-io/-/blob/master/README.md>`_ for more information.


Sample
======
:download:`Timeline sample </_static/samples/Sample.otio>`.

.. literalinclude:: /_static/samples/Sample.otio
   :language: json
   :emphasize-lines: 12,15,18,21,29,32-40,52,59,67,88-99
   :linenos:


Python
======
If the `VSE IO`_ add-on is installed and enabled.

.. highlight:: python
.. code-block:: python

    bpy.ops.sequencer.import_editing(filename="/path/to/timeline.otio")


------------------
Sequences features
------------------
When a media is loaded in the editor's timeline, a ``sequence`` is created. This sequence can hold several information which enables some features.
This information is held by the `Custom Properties`_ of a ``sequence``. A custom property can be set by the user through the UI, but also through the API.

.. note:: If you use the VSE IO add-on, the sequence information must be set on the ``Clip`` in the OpenTimelineIO nomenclature.

Review authorization
====================
Define if the current media is allowed to be reviewed. The `Sequence.lock` attribute from Blender is used, locking a sequence will disable notes edition for the sequence.

``media_sequence.lock = bool``

.. code-block:: python

  media_sequence.lock = False

.. _`media-info`:

Media info displayed in Preview
===============================
Display media info in Preview.

``media_sequence["stax_media_info"] = dict``

.. code-block:: python

  media_sequence["stax_media_info"] = {
      "Shot": {
          "Assets": ["Camel", "Pinguin", "Berry"],
          "Due date": "01/02/03"
          },
      "Version": {
          "Artist": "Jean-Michel"
      }
  }

.. figure:: /_static/images/MediaInfo_Sidebar.png
   :alt: Media information in preview

   Media information in preview


Trouble shooting
================

Any trouble?
------------
Try to launch Stax using the :ref:`command line` and share the log on the `discord`_ to ask for assistance.

.. _discord: https://discord.gg/fceFNFH
.. _`VSE IO`: https://gitlab.com/superprod/stax_suite/vse-io
.. _`Custom Properties`: https://docs.blender.org/manual/en/latest/files/data_blocks.html#files-data-blocks-custom-properties