.. _`reviews`:

==========================
Reviews
==========================

Regarding the reviews, Stax *links* them, it's not an import. This way an existing review is updated at every :ref:`publish <publish reviews>`.
This allows a continuous synchronization between all the users without any other third party software.

All Stax's reviews information is read and written using `OpenReviewIO <https://pypi.org/project/openreviewio/>`_, an open source standard, to ensure compatibility.

A review is linked to a sequence when ``orio_review_path`` is set in the `Custom Properties`_ and targets a review.

.. _`Custom Properties`: https://docs.blender.org/manual/en/latest/files/data_blocks.html#files-data-blocks-custom-properties

The reviews are linked to the selected or all sequences depending on the ``Link Type`` setting.

There are two distinct behaviours: 

* ``NEXT_TO`` `(default)`: Try to find the review in the same directory of the media file. Following the naming convention: ``media_name.ext.orio``. For example, a media ``/path/to/my_video.mov`` must have its review ``/path/to/my_video.mov.orio``.

* ``DIRECTORY``: Try to match the reviews in a directory using the ``media`` field of the reviews. If the field matches the sequence's media file path, the review is linked to it. Basically this directory contains several ``*.orio`` folders.


.. _`link reviews`:

Link
====
Build the available reviews from the selected or all sequences in the timeline under a metasequence ``stax.{original_sequence_name}``.

1. Select the sequences you want to link (using ``Shift + Left click`` or the box selection with ``B`` for example)

.. figure:: /_static/images/SelectedSequences.png
        :alt: Selected Sequences
        
        Selected Sequences

2. Click on ``Link Selected Sequences`` or ``Link All Sequences``

.. figure:: /_static/images/ReviewsMenu.png
        :alt: Reviews Menu
        
        Reviews Menu

3. *(DIRECTORY mode only)* Select the directory where the related reviews are stored

.. figure:: /_static/images/LinkReviews_Browser.jpg
        :alt: Link Reviews Browser
        
        Link Reviews Browser

Python
------
:attr:`~stax.ops.ops_reviews.SEQUENCER_OT_link_sequences_reviews`


.. highlight:: python
.. code-block:: python

    # /!\ Select the sequences first

    # NEXT_TO mode
    bpy.ops.sequencer.link_sequences_reviews(action="LINK")

    # DIRECTORY mode
    bpy.ops.sequencer.link_sequences_reviews(action="LINK", directory="/path/to/directory/")


Unlink
======
Remove for the selected sequences the built reviews deleting the ``stax.`` metasequence and restoring the original media sequence.

1. Select the sequences you want to unlink (using ``Shift + Left click`` or the box selection with ``B`` for example)

.. figure:: /_static/images/SelectedSequences.png
        :alt: Selected Sequences
        
        Selected Sequences

2. Click on ``Unlink Sequences Reviews``

.. figure:: /_static/images/ReviewsMenu.png
        :alt: Reviews Menu
        
        Reviews Menu

Python
------
:attr:`~stax.ops.ops_reviews.SEQUENCER_OT_link_sequences_reviews`


.. highlight:: python
.. code-block:: python

    # /!\ Select the sequences first

    bpy.ops.sequencer.link_sequences_reviews(action="UNLINK")


Update
======

To simply update the selected reviews with the modifications which have been made externally, just click on the **Update** button.

.. figure:: /_static/images/SessionMenu_Update.png
   :alt: The marvellous update button

   The marvellous update button

Stax will load any new note which has been made to the reviews since the previous update.

Python
------
:attr:`~stax.ops.ops_reviews.SEQUENCER_OT_update_reviews`


.. highlight:: python
.. code-block:: python

    # /!\ Select the sequences first

    bpy.ops.sequencer.update_reviews()


.. _`publish reviews`:

Publish
=======
Once all the notes have been edited for the medias and the review session is done, it's time to publish the reviews of the :ref:`reviewed media sequences <reviewed>`.

.. figure:: /_static/images/PublishReviews_Button.jpg
   :alt: Publish Button

   Publish Button

If the sequence has a ``orio_review_path`` custom property set, the review will be written to this path.

But, if this field is not fulfilled the publish behaviour relies on the ``Link Type`` setting, like :ref:`linking the reviews <link reviews>`.

There are two distinct behaviours: 

* ``NEXT_TO`` `(default)`: Write the review in the same directory of the media file. Following the nomenclature: ``media_name.ext.orio``. For example, a media ``/path/to/my_video.mov`` will have its review ``/path/to/my_video.mov.orio``.
* ``DIRECTORY``: Write the reviews in the target directory selected with the browser.

If new notes are created for a media with an existing review but this review hasn't been linked during this session, the existing review will be updated and new notes will be written into it.

Stax uses `OpenReviewIO`_ to :ref:`load <link reviews>` and publish review files.

.. _OpenReviewIO: https://pypi.org/project/openreviewio/

Python
------
:attr:`~stax.ops.ops_reviews.STAX_OT_publish_reviews`


.. highlight:: python
.. code-block:: python

    # NEXT_TO mode
    bpy.ops.scene.publish_reviews()

    # DIRECTORY mode
    bpy.ops.scene.publish_reviews(directory="/path/to/directory/")


Trouble shooting
================

My review isn't visible over the media
--------------------------------------

Stax matches the review to the corresponding media using the media file path.

If the expected review isn't visible after linking when the media is displayed, you must check the media file path is the same of the ``media_path`` in the review file.

#. Go into the corresponding ``my_reviews_dir/my_media_name.mkv.orio`` review folder.
#. Open ``review.orio`` file. It's basically XML.
#. Make sure the value of ``media_path`` matches the media file path.