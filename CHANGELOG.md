## Unreleased

## Fixes
- Advanced drawing image sequences markers are not displayed as "pending".
  - Created `Sequence.pending` boolean.

## 3.1.1
* **Release date:** 17/01/2022
* **Blender:** 2.93.*
* **OpenReviewIO:** 0.0.43

### Fixes

- "Sound" named track is not automatically exported.

## 3.1.0
* **Release date:** 07/01/2022
* **Blender:** 2.93.*
* **OpenReviewIO:** 0.0.43

### Changes

- Renamed `show_text_annotation` to `show_orio_annotation`.

### Features

- Show image annotations markers in sequencer.
  - Pending image annotations are taller and a bit transparent.
- Panel to list all image annotations for the displayed media.
  Click on the number to go to the first frame of the annotation.

### Refactor

- `build_sequence_overlays` has been split into `build_sequence_status` and `build_sequence_annotation_markers`.

### UI
- Image annotation marker color for both actual and pending can be customized in configuration.

## 3.0.1
* **Release date:** 23/12/2021
* **Blender:** 2.93.*
* **OpenReviewIO:** 0.0.43

### Documentation

- Configuration: Video Cache Memory.

### Fixes

- Unlink sequence review at sequence editor's root fails.
- Publish first review without setting a status fails.

## 3.0.0
* **Release date:** 21/12/2021
* **Blender:** 2.93.*
* **OpenReviewIO:** 0.0.43

### Changes

- `scene.update_session` renamed as `sequencer.update_sequences_reviews` and updates only the selected reviews.
- `sequencer.update_sequences_reviews` run at the end of `sequencer.link_sequences_reviews` to build/edit the reviews sequences.
- `get_text_contents` renamed as `extract_from_text_contents`.
- `utils_note.py` deleted.
- `.restore` system is abandonned.
- Removed `scene.change_stax_configuration` operator.
- Removed `sequencer.highlight_updated_reviews` calls because too confusing and unstable. Operator kept if appears to be a necessary feature in the future.
- Removed `clean_outdated_cache`.
- `get_parent_metas` renamed as `get_metas_hierarchy`.
- Removed `utils_openreviewio.py`.
- Annotation name is `{media.name}_{layer}_{frame}_uuid[:4]`.
- Removed `"review_enabled"` feature.
  - Removed deprecated `SEQUENCER_OT_LockReview` operator.
- Removed useless `update_tracks`.

### CICD
- Removed `test_ops.py`content because existing tests were about removed operators.
- `pip install openreviewio` into `python` directory to ease installation.

### Documentation

- Updated for 3.0
- A difference is made between `timeline` (area where the sequences are in Blender) and `editing` (set of medias with cut information).
- Use `sequence.lock` to disable notes edition.

### Features

- Timeline export use a `Shot` concept to display or not a sequence when exporting.
- Timeline export can handle channels if `scene.tracks` is not fulfilled.
- Added `stax_meta` property for metas created by Stax to be sure we don't break the original timeline.
- Override `meta_toggle` as `toggle_meta` to toggle only native metas and not Stax's ones.
- `sequencer.link_sequences_reviews` sets `orio_review_path` to sequences depending on `link_type`.
- `sequencer.link_sequences_reviews`:
  - actions: `LINK` and `UNLINK`
  - link types: `NEXT_TO` and `DIRECTORY`
- Created `build_media_reviews` to build media reviews as VSE sequences.
- Created `build_notes_sequences` to build notes as VSE sequences with their contents.
- Created `extract_media_reviews` to build openreviewio `MediaReview` object from a review sequence.
- `scene.clear_drawings` renamed as `scene.clear_pending_note` and clears the whole pending note instead of only the drawings.
- `wm.change_workspace` operator and menu to change the workspace.
- Added `current_status` and `pending_status` to media sequence.
- Autosave for config when a setting is changed.
- Hypertext links are clickable from the UI and open a browser's window.
- `get_current_sequence` new `channel` argument to specify channel.
- Timeline export new render track auto select next channel of last created render track.
- Created `get_sequence_channel_frame` to get sequence at channel and frame.
- Notes are got from currently displayed media.
- Adv. Drawing: Sound got from source scene and created into drawing scene.

### Refactor

- Removed internal timeline Stax architecture: `Track`, `Element`, `Version`.
- Refactor to use `meta.sequences` as much as possible instead of `meta_toggle` in chain.
- `scene.current_track` refactor to guess the possible tracks to select. Allow the possibility to set a custom name to the tracks without loading an editing file.
- Can be used without `VSE IO` add-on but warns the user when missing to install it.
- Authentication operator changed to a `StringProperty` "author_name".
  - `utils_ui.draw_author` function created to be easily overriden for customization.
  - Whole password logic deleted because up to pipeline.
- Removed Callbacks system.
- `review_linked_dir` moved to `reviews_linked_directory`.
- Split `scene.publish_reviews` into `build_pending_notes` and `export_notes` for easier process customizing.
- All pending note information is held by a pending note sequence under the review sequence.
- Replies are managed the same way as usual notes to stick to OpenReviewIO design.
- `sequencer.show/hide_reviews_drawings` (un)mute only the review sequence.
- `autosave` moved to `load_handler`.
- `draw_sequences_statuses` as GLSL shader for better control over colors and drawing order.
- Comparison alpha type updates timeline using `foreach_set`.
- Progressive comparison update for blend type and wipe using timer. Updating by chunks from current sequence and spreading both sides form current channel and then to below.
- Emphasize drawings use camera instead of background object.
- Split `wm.advanced_drawing` into `wm.start_advanced_drawing`, `wm.cancel_advanced_drawing` and `wm.complete_advanced_drawing` for more reliability.
- `build_pending_notes` gets reviewed sequences only.

### UI

- `scene.reviews_linked_directory` displayed in Stax menu.
- Workspaces tabs removed.
- Report messages moved to topbar.
- `sequencer.update_sequences_reviews` added to topbar `Session` menu.
- Added many Blender's `File` menu entries to `Session` menu to Open/Save existing sessions.
- `Abort` and `Publish` buttons moved to topbar.
- Tools in Preview's left bar are always visible but disabled when review isn't available.
- `toggle_annotations_keyframes` moved to Annotate tool context.
- 'Unsupported media type for review' message added.
- Status change disabled for unsupported media type.
- Added onion skin for annotate tool.
- Pending notes not displayed in main notes thread.
- `View all clips` renamed `Frame all` to match Blender's naming.
- Removed `write_comment` in `Text notes` panel.
- Display sidebar button in right panel.

### Shortcuts

- `Ctrl/Cmd + Enter` to confirm a text comment.


## 2.5.0
* **Release date:** 02/07/2021
* **Blender:** 2.83.*
* **OpenTimelineIO:** 0.11.0
* **OpenReviewIO:** 0.0.37

### Changes

- Cleaned dead code.
- `current frame only` renamed to `set range to current frame`.

### Features

- Add a link to documentation in the `Stax` menu.
- Created `scene.media_current_frame` property to set the global time using the media time.
- Allow the use of environment variables in media paths.

### UI
- Display current frame number in media time into Note Edition panel and space "time".

## 2.4.4
* **Release date:** 19/05/2021
* **Blender:** 2.83.*
* **OpenTimelineIO:** 0.11.0
* **OpenReviewIO:** 0.0.37

### Bugfixes

- Nested elements in `stax_media_info` fail for dicts.

## 2.4.3
* **Release date:** 17/05/2021
* **Blender:** 2.83.*
* **OpenTimelineIO:** 0.11.0
* **OpenReviewIO:** 0.0.37

### Bugfixes

- Nested elements in `stax_media_info` fail for lists.
- `New Session` button dynamically loads current app template's name.
- Docs CICD.
- Complete drawing deletes the media sequence and crashes.

## 2.4.2
* **Release date:** 07/05/2021
* **Blender:** 2.83.*
* **OpenTimelineIO:** 0.11.0
* **OpenReviewIO:** 0.0.37

### Bugfixes

- Edit adv. drawings creates duplicates of `temp` meta which leads to make `publish reviews` crash.

## 2.4.1
* **Release date:** 31/03/2021
* **Blender:** 2.83.*
* **OpenTimelineIO:** 0.11.0
* **OpenReviewIO:** 0.0.37

### Bugfixes

- Save blend file into stax app template instead of cache directory.
- Stax Vanilla: whole timeline is emptied when abort review.
- UI: Error message in Adv drawing because of missing icon.

### Changes

- Created `utils_note.py` to have all related note operations and created functions to clear pending note contents on a sequence.
  - `clear_advanced_drawings`
  - `clear_annotate`
  - `clear_text_content`

### Features

- Warn the user if the blender version is wrong and stop Stax's startup.

## 2.4.0
* **Release date:** 23/03/2021
* **Blender:** 2.83.*
* **OpenTimelineIO:** 0.11.0
* **OpenReviewIO:** 0.0.37

### Bugfixes

- Can't draw over a movie by default.
- Publish reviews of movies added at timeline's root.
- `clear_drawings` doesn't work for dropped media.
- Publish reviews with adv. drawing of a dropped media fails.
- Abort doesn't work for dropped media.
- Publish reviews with annotate of a dropped media fails.

### Changes

- scene `frame_end` and `frame_start` are enabled.

### CICD

- Use blendergrid/blender images. Had to fix some steps.

### Features

- Display `stax_media_info` property into Preview.
- Removed `select_tracks_to_display`.

### UI

- Added `Session` menu to start new, save, update, manage timeline or reviews and quit.
- Stax button moved to left.
 
## 2.3.0
* **Release date:** 01/03/2021
* **Blender:** 2.83.*
* **OpenTimelineIO:** 0.11.0
* **OpenReviewIO:** 0.0.36

### Bugfixes
- `cache_directory` as `''` by default instead of loading temp dir. Cleaned `scene.user_preferences` from `startup.blend`.
- Rendered drawings look pixelized.
- Rendered published PNG of annotate tool have a less saturated color.
- Publishing error when no review for media.
- Emphasize drawings not synchronized in main view because of sound strips.
- Stax without linked reviews: Published reviews aren't loaded back into timeline.
- User settings: using an empty value for cache directory falls back to the default cache directory instead of the current working directory.
- Toggle Versions: When timeline is loaded, the versions of the media from the highest track are loaded instead of the ones of the current sequence.
- Main track empty name raises an error and cancels timeline loading.
- Toggle versions set preview range end to one frame after the end of the sequence.
- Timeline export: only one track renders the track name in the middle of the media instead of at the bottom of the image.
- Default color space is set to `STANDARD`.
- Auto color space for images instead of forcing `XYZ`.
- `user_login` default set as current session's user, got from system information.
- Status is not correctly written to published reviews.

### Changes
- Publish reviews automatically publishes into linked directory by default. If no review directory has been set, publishing prompts to select a directory.
- Draw sequencer: draw status on current meta channel in `y` but of media sequence in `x`.
- Media opacity is now global, not related to the modified media.
- Clear Drawings button moved to Preview toolbar.
- Abort/Publish reviews buttons moved to Preview topbar.
- Disable "Load/Link" buttons when review session is active.
- Delete comment icon changed from "REMOVE" to "TRASH".
- Created `ui_notes.py` to handle ui classes regarding ORIO notes.
- Created `Review.as_orio_review` to get the Stax Review as an OpenReviewIO object.

### Documentation
- Doc for TDs.
  - `Features` section improvements.
  - Added sample timeline.
- `Reviews linking` documentation.
- Rewritting of `getting started` part with more explanations and consistent with Stax vanilla.

### Features
- Visual feedback for "reviewed but not published": `*` on sequences in TL and in Preview, before media name.
- Automatic tests using Pytest.
- Wipe to compare two media. Three parameters: Position, Angle, Blur. **Known limitation: works only with the currently displayed track.**
- Text comments and text annotations are managed the same way, as text contents.
- Text annotation:
  - Text annotations added to the comments thread panel.
  - Clicking on the text moves the playhead to the frame. `Shift + click` to also set the preview range and loop the annotation.
- Notes panel:
  - `Comments thread` renamed as `Notes`.
  - Text annotation:
    - Pending note panel above the notes panel.
    - Clicking on the text moves the playhead to the frame. 
    - Click onto the chronometer icon or `Shift + click` on the text set the preview range and loop the annotation.
    - New comment sets by default the preview range on the current sequence and zoom into it. If the `comment range` is contained into sequence's boundaries, it's a TextAnnotation, else a TextComment.
- Added `Purge Cache` operator to be able to purge the cache from startup.
- Added CICD pipeline:
  - Tests
  - Docs
  - Pages (using Pelican and default theme for now)
  - Packing : Create an Application Template `.zip` easily installable by Blender
  - Release : Release the `.zip` as an item to DL.
- Removed `select_tracks_to_display`.

### Optimization
- Statuses drawing optimization, between x2 and x4.

### Refactor
- Most of the timeline operations are based on the `media_sequence` to avoid ambiguous behaviour with meta.
- Make `get_raw_sequence/get_media_sequence` fall back on the currently display Movie or Image sequence if nothing else is caught.
- Refactor drawing annotations with `sequence.frame_` attributes.
  - `sequence.frame_start` is used to calculate frame in the media time.
  - `sequence.frame_offset_start` is used to set the frame offset.
  - `sequence.frame_final_start` is used to calculate frame in the timeline time and handle sequence positioning.
- Moved meta making of video + sound after the Aspect ratio preservation for ensuring sound offset and being able to rely on raw sequence `frame_` attributes.
- `raw_sequence` renamed as `media_sequence`.
- `get_visible_sequences` is more reliable when overlapping sequences.
- `get_collection_entity` supports collections using `.new("name")` instead of only `.add()`.
- `load_timeline` function moved into `utils_timeline` to be used by linker coders independently from `LoadTimeline` operator.

### UI
- Edit text note into Adv. Drawing mode, same buttons as in Preview into `Main` workspace.
- `waiting review` status changed from grey to orange. Also harmonized the other colors.
- `Stax configuration`: Added `Purge Cache` operator.

## 2.2.1
* **Release date:** 02/12/2020
* **Blender:** 2.83.*
* **OpenTimelineIO:** 0.11.0
* **OpenReviewIO:** 0.0.36

### Bugfixes
- Display media name in preview: Error message when no sequence.
- `Update`, `Load Timeline` and `Link Reviews` buttons still visibles in Adv. Drawing mode when they shouldn't.
- Cache errors when the cache is set as relative path -> Forced to be absolute when config is saved.
- Sequencer is not reloaded correctly when input otio timeline changes.
- Crash when session was previously quit without exiting drawing mode.
- Pan with `Middle Mouse` doesn't work on windows.

## 2.2.0
* **Release date:** 30/11/2020
* **Blender:** 2.83.*
* **OpenTimelineIO:** 0.11.0
* **OpenReviewIO:** 0.0.36

### Bugfixes
- Undo after entering Adv. Drawing breaks Stax.
- Reviews of locked sequences (review disabled) are published when they shouldn't.

### Changes
- Versions tracks created when toggling versions are renamed "-1, -2, -n...".

### Changes
- `BlenderExecPath` renamed as `STAX_BL_PATH` to fit env var naming convention and being more explicit as it is a Stax used env var.

### Features
- Sound is played in Advanced Drawing mode. 
- `Update` button is disabled when a review session has started, until the user `Aborts` or `Publishes`.

### UI
- Currently displayed media name (without extension) is displayed in preview.

## 2.1.2
* **Release date:** 23/11/2020
* **Blender:** 2.83.*
* **OpentimelineIO:** 0.11.0

### Refactor
- Startup blender settings are made into code instead of modifying `startup.blend`. This binary modifications are now done only when there is no other alternative and with a huge caution.
- Background image in Adv. Drawing is now handled by an empty object named "Background" displaying an image, added in `startup.blend`. 
  - The background image of the camera is a white generated image to ensure emphasize drawings without relying on the world's color (too much erratic influence over shading).

### Changes
- Viewport doesn't take world color into consideration to avoid onion skin and fill white artifacts.
- Drawings to front to avoid drawing artifacts.

### Bugfixes
- Emphasize drawings doesn't work as expected.

## 2.1.1
* **Release date:** 20/11/2020
* **Blender:** 2.83.*
* **OpentimelineIO:** 0.11.0

### Bugfixes
- Wrong building for preserved ratio when several media.
- Adv. Drawing color stroke too dark.

## 2.1.0
* **Release date:** 18/11/2020
* **Blender:** 2.83.*
* **OpentimelineIO:** 0.11.0

### Refactor
- Stax's keymap is defined by a file `stax_keymap.py` and loaded at startup.

### Changes
- Passepartout value in Adv. Drawing is fully black by default to be consistent with the Sequencer's Preview.

### Bugfixes
- Deleted tracks when selected tracks to display are remaining at the top of the timeline.
- Do not reload the linker modules several times.
- Stax exports only the first externally edited image, and ignores all the others.

### Features
- Video `Version`s with `include_sound=True` will import their sound track from video files in the sequencer.
- Can emphasize drawings by changing the color balance of sequences, from normal to fully white.
  - Value consistent between `Main` and `Adv. Drawing` workspaces.
- Video `Version` (version `Track`) will be trimmed if its `source_range` is set.
  Conversely, annotations are saved in **media time**, taking the offset into account.
- Import still images like videos from otio timeline.
- `Version` with `preserve_ratio=True` will make Stax get the ratio from the media file and preserve the ratio in the viewport.
- Edit mode in Advanced Drawing to modify drawings.

#### Shortcuts
- Main:
  - `Shift + LMB`: Select several sequences -> Button to set preview range on selected`
  - `Ctrl + arrow up/down`: Change current displayed track, loop when reaching boundaries.
  - `NUMPAD #` keys to select tracks by index.
- Advanced Drawing:
  - `arrow up/down`: jump keyframe backwd/fwd


## 2.0.0
* **Release date:** 14/10/2020
* **Blender:** 2.83.*
* **OpentimelineIO:** 0.11.0

### Files layout
- Files have been moved based on a more explicit layout.

### Features
- Stax notes and status cache is in OpenReviewIO format.
- Merge Note and Player modes -> 'Main' workspace.
  - Drawing directly in viewer as vectorial and editable with the Annotations tool.
    - Editable by clicking on the `sequencer.toggle_annotations_keyframes` operator.
  - Possibility to export the current frame in an external image editor.
  - Change the status of the media review.
  - Clear all drawings made over a media.
- All python dependencies are installed in stax template directory.
- Session system
  - Load any timeline with filebrowser.
  - Session autosave delay is customizable.
  - Publishing the reviews with a button/operator, open a filebrowser to export the reviews in a directory. Use the callback if set.
- Callbacks
  - Uses optional callbacks instead of a linker.
  - Overriding any Blender or Stax class from callbacks script by running the `register()` function.
- Review can be disabled by setting 'review_enabled' to False in OTIO timeline's version track.
  - Publish session ignores review disabled sequences.
  - The UI changes and notifies the user.
- User can set/clear a preview range by clicking a button in the sequencer and modify the start/end values. There are two ways to do so:
  - Box Select `P` (default Blender): Set the Preview range doing a box selection in the sequencer.
  - Current `Shift + P`: Set the Preview Range around the current media (white highlighted one)
  - Clear Preview Range `Alt + P` (default Blender).
- Dev tools:
  - `wm.mouse_location` operator to print mouse coordinates when clicking.
- Button "Edit Image in external editor" opens a filebrowser if no external image editor is selected and allows the user to pick the executable.
- Show the changes in reviews (statuses and notes) that have changed by selecting the related sequences in the sequencer and zooming on them.
- Advanced Drawing using Grease Pencil:
  - Dedicated Workspace and simple UI.
  - Editable drawings.
  - Slider to change `Fill opacity` value of all fills.
- Text comments
  - Write text comments related to the whole media.
  - Thread to display all, write new.
  - Tool button to write a new one.
  - While not published, a reply to a text note is editable.
  - Pending comment or reply as greyed out usual comment/reply.

- Operator `bpy.ops.scene.link_reviews()` to target a directory from which Stax will get the reviews. Also has an option to copy the reviews in Stax's cache. Can be used in CLI `bpy.ops.scene.link_reviews(directory="...", copy_to_cache=True)`.
- UI:
  - `Stax` button to show informations on clicking. Operator `wm.open_weburl` to open URLs added to allow the user to go directly to the **Support** channel or to the **Repository**.
  

### Changes
- `scene.tasks` renamed as `scene.tracks`.
- `track.remove()` renamed as `track.delete()` and now move above sequences down when a track is deleted.
- `linker` renamed as `callback`.
  - If no callback script don't load the first one by default.
- Custom Callbacks classes have to inherit from Callbacks class
- The playback/render frame rate is set by adding a 'frame_rate' metadata to the OTIO Timeline.
- Source media copy to Stax' cache is optional and `False` by default. Has to be set in the user configuration.
- Read/Write_user_config() refactored for being more consistent and used only when necessary.
- UI:
  - The UI has been totally cleaned of Blender remaining UI.
  - Workspaces uniformization and simplified UI (buttons without text for occasional actions).
  - Custom icons import for some tools and `.blend` provided with a script to create our own icons.
- Dependencies are installed from a list instead of writting a try/except for every module.
- `install_lib` renamed as `install_dependency`.
- `reset_cache` renamed as `delete_session_cache`, cleaned and doesn't delete ORIO reviews, this has to be handled by the pipeline.
- Timeline update refactor : reviews loading is after the timeline building.
- Timeline operators renamed for consistency.


### Bugfixes
- Side by side (a.k.a `wm.splitview`) operator.
- `clean_outdated_cache` works with the Stax 2.0's architecture.
- Getting the current displayed sequence is now possible with `scene.sequence_editor.active_strip` and not calling every time `get_current_sequence()`.
- `scene.add_empty_gp_frame` adds empty frame on the current layer instead of the "Note" one.

### Documentation
- User documentation written. Go to `docs/` and run either `make html` on Unix or the `make.bat` on Win.
- Added a 'video' directive to optimize GIFs loading.
- Base of Dev documentation.

## 1.6.0

* **Release date:** 
* **Blender:** 2.82a
* **OpentimelineIO:** 0.11.0

### Feature

- Add an auto login system
- Last session's update saved in scene.stax_info.session_last_update as UTC in ISO.

### Changes
- 'stax_version' renamed 'version' and accessible via 'scene.stax_info.version'.

____

## 1.5.1

* **Release date:** May 05 2020
* **Blender:** 2.82a
* **OpentimelineIO:** 0.11.0

### Bugfixs

- Edit Externally view_transform correctly set to Standard

____

## 1.5.0

* **Release date:** Apr 20 2020
* **Blender:** 2.82a
* **OpentimelineIO:** 0.11.0

### New features

- Stax cache is automatically purged if the current version of Stax doesn't match with the `stax_version` of the cache file.

### Changes

- Pass clip object instead of clip.name to get_webpage_url
- Add an icon to the show/hide annotation to let the user know the state.

### Bugfixs

- Remove overlap in annotations due to select_grouped(type='OVERLAP')
- Directly show saved annotation

____

## 1.4.1

* **Release date:** Apr 16 2020
* **Blender:** 2.82a
* **OpentimelineIO:** 0.11.0

### UI

- Rename "Note Mode" button into "Start Note"

### Bugfixs

- Specify module version using == instead of =
- Add install_lib to __init__.py as it is needed for otio.
- Unregister class RenderTask

____

## 1.4.0

* **Release date:** Apr 06 2020
* **Blender:** 2.82a
* **OpentimelineIO:** 0.11.0

### How to install

* Need Blender 2.82a
* Remove cfg.json from $APPDATA/Roaming/Stax (windows) or Home/stax (Unix)

### New features

* New annotations system
  * Overlay
  * Duration
  * Layers
  * Show/hide annotations (buttons and toggle)
* New environment variables system for the linker

### Enhancements

* Stax now uses Blender 2.82a
* Linker and Core separation
* Timelines are loaded by their name
* Faster annotation exports
* Reset timeline cache also remove OpenReview cache

### Bugfixs

* OpentimelineIO specified as 0.11.0
* "project_name" not specified in cfg.json doesn't create an error anymore

### Others

* Lots of refactor
  * Change the files architecture
  * Rename functions, variables, objects
  * Metadata system
  * frame_start and duration moved from element to version
  * Tasks are merged into one list
  * ...
  * Demo linker removed from Stax
  * SHIFT + click data added to Update tooltip
 
 ### Linker
 
* Linker is now a Blender PropertyGroup
* get_user_tasks() is now get_tasks()
* get_episode() is now get_timeline() -> Changes on the attributes
* Approve/reject_version() -> Changes on the attributes
* ...

*See ProductionManager_Linker.py for changes*
