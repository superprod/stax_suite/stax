# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to timeline
"""
import bpy

from stax.utils.utils_core import (
    get_context,
)
from stax.utils.utils_timeline import (
    get_available_tracks,
    set_preview_range_from_sequences,
)
from stax.properties.properties_core import frame_change_update


class SEQUENCER_OT_HighlightUpdatedReviews(bpy.types.Operator):
    """Highlight sequences depending on the 'updated' parameter of their related review by selecting them

    TODO Refactor it if seems necessary in upcoming releases.
    """

    bl_idname = "sequencer.highlight_updated_reviews"
    bl_label = "Highlight Updated Reviews"

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def invoke(self, context, event):
        # Sentinel
        if not context.scene.reviews:
            self.report(type={"WARNING"}, message="No any loaded reviews.")
            return {"CANCELLED"}

    def execute(self, context):
        scene = context.scene

        # Sentinel
        if not scene.reviews:
            return {"CANCELLED"}

        # Deselect all sequences
        bpy.ops.sequencer.select_all(
            {"scene": bpy.data.scenes["Scene"]}, action="DESELECT"
        )

        # Select sequences
        for sequence in scene.sequence_editor.sequences:
            sequence.select = sequence.get("updated", False)

        return {"FINISHED"}


class SEQUENCER_OT_set_preview_range_selected_sequences(bpy.types.Operator):
    """Set the preview range to the selected sequences"""

    bl_idname = "sequencer.set_preview_range_selected_sequences"
    bl_label = "Set Preview Range Selected Sequences"

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def execute(self, context):
        # Set preview range
        set_preview_range_from_sequences(context.selected_sequences)

        # Deselect all
        bpy.ops.sequencer.select_all(action="DESELECT")

        # Set current displayed sequence as active
        frame_change_update(context.scene)

        return {"FINISHED"}


class SEQUENCER_OT_offset_preview_display_channel(bpy.types.Operator):
    """Change preview display channel by an offset"""

    bl_idname = "sequencer.offset_preview_display_channel"
    bl_label = "Offset Preview Display Channel"

    channel_offset: bpy.props.IntProperty(name="Channel to display", default=1)

    def execute(self, _context):
        preview_area = get_context("Main", "PREVIEW").get("space_data")

        # Apply offset
        target_channel_index = preview_area.display_channel + self.channel_offset

        # Get available tracks
        available_tracks = get_available_tracks()

        # Make the selection loop
        if target_channel_index > len(available_tracks) - 1:
            preview_area.display_channel = 0
        elif target_channel_index < 1:
            preview_area.display_channel = len(available_tracks)
        else:
            preview_area.display_channel = target_channel_index

        return {"FINISHED"}


class SEQUENCER_OT_show_orio_annotation(bpy.types.Operator):
    """Click to move the playhead to first frame of the annotation.\nShift + Click to set the preview range"""

    bl_idname = "sequencer.show_orio_annotation"
    bl_label = "Show ORIO Annotation"

    frame_start: bpy.props.IntProperty(name="Start frame", options={"SKIP_SAVE"})
    frame_end: bpy.props.IntProperty(name="End frame", options={"SKIP_SAVE"})
    set_preview_range: bpy.props.BoolProperty(options={"SKIP_SAVE"})
    move_playhead: bpy.props.BoolProperty(options={"SKIP_SAVE"}, default=True)

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def invoke(self, context, event):
        scene = context.scene

        # Shift click on a text annotation to set the preview range
        if event.shift:
            # If preview range already set and matching the current annotation, disable it, else set the new one
            if scene.use_preview_range and (
                scene.frame_preview_start == self.frame_start
                and scene.frame_preview_end == self.frame_end
            ):
                scene.use_preview_range = False
            else:
                self.set_preview_range = True

        self.execute(context)

        return {"FINISHED"}

    def execute(self, context):
        scene = context.scene
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip

        # Move playhead to starting frame of annotation
        if self.move_playhead:
            scene.frame_set(self.frame_start)

        # Set annotation preview range button only if wider than 1
        # (avoid 1 frame at sequence's start when annotation is out of boundaries because of handles)
        if self.set_preview_range and self.frame_end - self.frame_start > 1:
            scene.use_preview_range = True
            scene.frame_preview_start, scene.frame_preview_end = (
                self.frame_start,
                min(self.frame_end, current_sequence.frame_final_end),
            )

        return {"FINISHED"}


class SEQUENCER_OT_set_range_current_frame(bpy.types.Operator):
    """Set the range to the current frame"""

    bl_idname = "sequencer.set_range_current_frame"
    bl_label = "Set Range to Current Frame"

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def execute(self, context):
        scene = context.scene

        # Set preview range to current frame
        scene.use_preview_range = True
        scene.frame_preview_start, scene.frame_preview_end = (
            scene.frame_current,
            scene.frame_current,
        )

        return {"FINISHED"}


classes = [
    SEQUENCER_OT_HighlightUpdatedReviews,
    SEQUENCER_OT_set_preview_range_selected_sequences,
    SEQUENCER_OT_offset_preview_display_channel,
    SEQUENCER_OT_show_orio_annotation,
    SEQUENCER_OT_set_range_current_frame,
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.timeline = bpy.props.StringProperty(
        name="Source timeline",
        description="The session's editing is built from this timeline",
    )


def unregister():
    del bpy.types.Scene.timeline

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
