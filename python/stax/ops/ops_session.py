# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to user session
"""

import bpy


class WM_OT_watch_review_start(bpy.types.Operator):
    """Automatically detect when a review starts"""

    bl_idname = "wm.watch_review_start"
    bl_label = "Auto detect review start"

    def execute(self, context):
        context.window_manager.modal_handler_add(self)
        return {"RUNNING_MODAL"}

    def modal(self, context, event):
        # Consider the review is started when the user clicks on the annotate tool
        # and there are some drawings: frames with strokes
        if context.screen.get("current_tool", "").startswith("builtin.annotate"):
            for layer in bpy.data.grease_pencils["Annotations"].layers:
                for frame in layer.frames:
                    if frame.strokes:
                        # Automatically start review
                        context.scene.review_session_active = True
                        return {"FINISHED"}

        return {"PASS_THROUGH"}


class ReportMessage(bpy.types.Operator):
    """Report given message in the given type"""

    bl_idname = "wm.report_message"
    bl_label = "Report message"

    message: bpy.props.StringProperty(name="Report Message", default="No Message")
    type: bpy.props.StringProperty(name="Report Type", default="INFO")

    def execute(self, context):
        if context.area:
            # Trick to display the message for sure
            context.window_manager.modal_handler_add(self)
            return {"RUNNING_MODAL"}
        else:
            self.report({self.type}, self.message)
            return {"FINISHED"}

    def modal(self, context, event):
        self.report({self.type}, self.message)
        return {"FINISHED"}


classes = [
    WM_OT_watch_review_start,
    ReportMessage,
]


def register():
    # Custom Properties
    bpy.types.Scene.is_timeline_loaded = bpy.props.BoolProperty(
        name="Is timeline loaded",
        default=False,
        description="Says if the timeline is correctly loaded.",
    )

    bpy.types.WindowManager.source_workspace = bpy.props.StringProperty(
        name="Source Workspace",
        default="Stax configuration",
        description="If called from other workspace, it keeps the source workspace to set it back at saving",
    )

    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    del bpy.types.WindowManager.source_workspace
    del bpy.types.Scene.is_timeline_loaded

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
