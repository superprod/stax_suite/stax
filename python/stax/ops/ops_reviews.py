# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to the reviews
"""
from datetime import datetime, timezone
from pathlib import Path
import shutil

import bpy
from bpy.props import BoolProperty, EnumProperty, StringProperty
from bpy.types import Operator

from stax.properties.properties_core import frame_change_update
from stax.utils.utils_core import (
    get_context,
)
from stax.utils.utils_cache import (
    get_temp_directory,
    save_session,
)
from stax.utils.utils_reviews import (
    build_media_reviews,
    build_pending_notes,
    clear_media_reviews,
    export_notes,
    link_media_reviews,
    get_link_types_items,
    read_media_reviews,
)
from stax.utils.utils_timeline import (
    get_media_sequence,
    get_reviewable_media_sequences,
    get_reviewed_media_sequences,
)


class SEQUENCER_OT_link_sequences_reviews(Operator):
    """(Un)link reviews to the selected sequences"""

    bl_idname = "sequencer.link_sequences_reviews"
    bl_label = "Link Sequences Reviews"

    filter_glob: StringProperty(
        default="*.orio",
        options={"HIDDEN"},
    )

    action: EnumProperty(
        name="Action",
        items=(
            (
                "LINK",
                "Link reviews",
                "Link selected sequences depending on the link type",
            ),
            ("UNLINK", "Unlink reviews", "Unlink selected reviews"),
        ),
    )
    all: BoolProperty(name="All", description="Link all sequences' reviews")
    link_type: EnumProperty(name="Link type", items=get_link_types_items())

    @classmethod
    def poll(cls, context):
        return context.sequences

    directory: StringProperty(
        name="Review as directory", subtype="DIR_PATH", options={"HIDDEN"}
    )

    def invoke(self, context, event):
        if self.action == "LINK":
            if self.link_type == "DIRECTORY":
                # File Browser
                context.window_manager.fileselect_add(self)
                return {"RUNNING_MODAL"}

            elif self.link_type == "NEXT_TO":
                return self.execute(context)

        elif self.action == "UNLINK":
            return self.execute(context)

    def execute(self, context):
        scene = context.scene

        # Tell to wait
        if hasattr(context, "window") and context.window:
            context.window.cursor_set("WAIT")

        # Sentinel
        if not self.all and not context.selected_sequences:
            self.report({"WARNING"}, "Please select sequences to (un)link reviews.")
            return {"CANCELLED"}

        # Sequences to link
        sequences_to_link = (
            get_reviewable_media_sequences()
            if self.all
            else {
                get_media_sequence(sequence)
                for sequence in context.selected_sequences
                if sequence.type != "SOUND"
            }
        )

        if self.action == "LINK":

            # Link reviews to selected sequences
            review_imported = bool(
                link_media_reviews(
                    sequences_to_link,
                    directory=self.directory,
                )
            )

            # Warn the user if nothing has been imported
            if not review_imported:
                self.report({"WARNING"}, "No any review found!")

            # Update session to create reviews
            bpy.ops.sequencer.update_sequences_reviews(all=self.all)

        elif self.action == "UNLINK":
            clear_media_reviews(
                get_reviewable_media_sequences()
                if self.all
                else {
                    get_media_sequence(sequence)
                    for sequence in context.selected_sequences
                }
            )

        # Update active parameters
        scene.frame_set(scene.frame_current)

        # Save session
        save_session()

        return {"FINISHED"}


class SEQUENCER_OT_update_sequences_reviews(bpy.types.Operator):
    """Update the reviews of the selected sequences.\nHold shift key to update all linked reviews.\nAlt key to build from scratch"""

    bl_idname = "sequencer.update_sequences_reviews"
    bl_label = "Update Sequences Reviews"

    all: BoolProperty(name="All", description="Update all reviews")
    reset: BoolProperty(name="Reset", description="Re-build reviews from the disk")

    def invoke(self, context, event):

        # Display loading for user
        if context.window:
            context.window.cursor_set("WAIT")

        # Key attributes
        self.all = event.shift
        self.reset = event.alt

        return self.execute(context)

    def execute(self, context):
        scene = context.scene

        # Keep update date
        update_date = datetime.now(timezone.utc).isoformat()

        # Sequences to update
        reviewable_sequences = (
            get_reviewable_media_sequences()
            if self.all
            else [
                get_media_sequence(seq)
                for seq in context.selected_sequences
                if seq.type != "SOUND"
            ]
        )
        selected_sequences = [
            seq for seq in reviewable_sequences if seq.get("orio_review_path")
        ]

        # Reset
        if self.reset:
            clear_media_reviews(selected_sequences, keep_review_link=True)

        # Build media reviews in timeline
        seqs_media_reviews = read_media_reviews(selected_sequences)
        build_media_reviews(seqs_media_reviews)

        # Save date
        context.scene.stax_info.session_last_update = update_date

        # Update active parameters
        scene.frame_set(scene.frame_current)

        # Save session
        save_session()

        return {"FINISHED"}


class SCENE_OT_abort_reviews(bpy.types.Operator):
    """Clear the created reviews"""

    bl_idname = "scene.abort_reviews"
    bl_label = "Abort Reviews"

    @classmethod
    def poll(cls, context):
        if (
            context.scene.review_session_active
            and context.screen.name != "Advanced drawing"
        ):
            return True

    def invoke(self, context, _event):
        # Ask to confirm
        return context.window_manager.invoke_confirm(self, _event)

    def execute(self, context):
        scene = context.scene

        # Clear tmp cache dir
        tmp_dir = get_temp_directory()
        shutil.rmtree(tmp_dir, ignore_errors=True)

        # Clear pending note contents for all sequences
        clear_media_reviews(
            filter(
                None,
                [
                    get_media_sequence(seq)
                    for seq in bpy.context.scene.sequence_editor.sequences
                ],
            ),
            pending_note_only=True,
        )

        # Close review session
        context.scene.review_session_active = False

        # Update active parameters
        frame_change_update(scene)

        # Watch any action considered to start the review session
        bpy.ops.wm.watch_review_start()

        # Set the picker as default tool back
        bpy.ops.wm.tool_set_by_id(get_context("Main", "PREVIEW"), name="builtin.sample")

        return {"FINISHED"}


class STAX_OT_publish_reviews(bpy.types.Operator):
    """Publish reviews of the current session"""

    bl_idname = "scene.publish_reviews"
    bl_label = "Publish Reviews"

    filter_glob: StringProperty(
        default="",
        options={"HIDDEN"},
    )

    filename_ext = ""

    directory: bpy.props.StringProperty(
        name="Outdir Path",
        description="Where reviews will be written",
        subtype="DIR_PATH",
    )

    @classmethod
    def poll(cls, context):
        if (
            context.scene.review_session_active
            and context.screen.name != "Advanced drawing"
        ):
            return True

    def invoke(self, context, event):
        # Tell to wait
        context.window.cursor_set("WAIT")

        # Behaviour depending on link_type
        reviews_properties = context.scene.reviews_properties
        if reviews_properties.link_type == "NEXT_TO":  # Ask to confirm
            return context.window_manager.invoke_confirm(self, event)

        elif reviews_properties.link_type == "DIRECTORY":
            if context.scene.reviews_linked_directory:  # Write in directory
                self.directory = bpy.context.scene.reviews_linked_directory
                return self.execute(context)
            else:  # Pick a directory
                context.window_manager.fileselect_add(self)
                return {"RUNNING_MODAL"}

    def execute(self, context):
        scene = context.scene
        scene.reviews_linked_directory = self.directory

        # Link reviews first to update if exist
        reviewed_sequences = get_reviewed_media_sequences()
        link_media_reviews(
            reviewed_sequences,
            directory=self.directory,
        )

        # Build media reviews in timeline
        seqs_media_reviews = read_media_reviews(reviewed_sequences)
        build_media_reviews(seqs_media_reviews)

        # Build pending notes
        edited_notes = build_pending_notes()

        #####################
        # Publishing
        #####################

        # Export reviews
        # --------------
        target_dir = Path(self.directory).resolve() if self.directory else None

        export_notes(edited_notes, target_dir)

        #####################
        # Cleaning
        #####################

        # Clear review of related media sequences, first element of edited_notes tuple
        clear_media_reviews([e[0] for e in edited_notes], pending_note_only=True)

        # Delete temp directory
        annotations_directory = Path(get_temp_directory(), "annotations")
        if annotations_directory.is_dir():
            shutil.rmtree(annotations_directory)

        # Update for published reviews
        build_media_reviews(
            [(media_seq, review) for media_seq, review, _e in edited_notes]
        )

        # Set the picker as default tool back
        override = get_context("Main", "PREVIEW")
        bpy.ops.wm.tool_set_by_id(override, name="builtin.sample")

        # Close review session
        context.scene.review_session_active = False

        # Save session and keep this new state as default
        save_session()

        # Watch any action considered to start the review session
        bpy.ops.wm.watch_review_start()

        return {"FINISHED"}


classes = [
    SEQUENCER_OT_link_sequences_reviews,
    SEQUENCER_OT_update_sequences_reviews,
    SCENE_OT_abort_reviews,
    STAX_OT_publish_reviews,
]


def register():

    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
