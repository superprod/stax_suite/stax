# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""
Every operator related to the general UI
"""

import bpy


class STAX_OT_change_workspace(bpy.types.Operator):
    """Change Workspace"""

    bl_idname = "wm.change_workspace"
    bl_label = "Change Workspace"

    workspace: bpy.props.StringProperty()

    def invoke(self, context, event):
        if event.shift:
            # Switch to scripting workspace
            context.window.workspace = bpy.data.workspaces["Console"]
        else:
            # Switch to workspace
            context.window.workspace = bpy.data.workspaces[self.workspace]

        return {"FINISHED"}


classes = [STAX_OT_change_workspace]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
