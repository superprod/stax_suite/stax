# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to version
"""
from pathlib import Path
from uuid import uuid4

import bpy
from bpy_extras.io_utils import ImportHelper

from stax.utils import utils_cache
from stax.utils.utils_core import get_context
from stax.utils.utils_reviews import (
    clear_media_reviews,
    get_active_pending_note_sequence,
    get_pending_notes_sequences,
    set_reviewed_state,
)
from stax.utils.utils_timeline import (
    display_media_annotations,
    get_media_sequence,
    get_review_sequence,
    get_source_media_path,
)
from stax.utils.utils_ui import (
    exit_advanced_drawing_ui,
)


class WM_OT_SelectExternalImageEditor(bpy.types.Operator, ImportHelper):
    """Select an external image editor through the file browser"""

    bl_idname = "wm.select_external_image_editor"
    bl_label = "Select an external image editor"

    filepath: bpy.props.StringProperty(
        name="Executable path",
        subtype="FILE_PATH",
    )

    def execute(self, context):
        # Set the path to the image editor Blender's path
        bpy.context.preferences.filepaths.image_editor = self.filepath

        # Save the preferences
        bpy.ops.wm.save_userpref()

        # Notify the user
        editor_name = Path(self.filepath).stem
        bpy.ops.wm.report_message(
            type="INFO",
            message=f"{editor_name} has been set as External Editor. Click again on the button to edit the current frame.",
        )

        return {"FINISHED"}


class MediaSequenceOperator:
    """Main operator for media sequence related operators"""

    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        active_strip = context.scene.sequence_editor.active_strip
        if active_strip and not active_strip.lock:
            return True

    def invoke(self, context, _event):
        # Tell the impatient user to wait
        context.window.cursor_set("WAIT")

        # Run process
        self.execute(context)

        return {"FINISHED"}

    def execute(self, _context):
        return {"FINISHED"}


class WM_OT_edit_image_externally(MediaSequenceOperator, bpy.types.Operator):
    """Edit current image with external image editor"""

    bl_idname = "wm.edit_in_image_editor"
    bl_label = "Edit in external image editor"

    edited_image = None

    @classmethod
    def poll(cls, context):
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        if current_sequence and not current_sequence.lock:
            return True

    def execute(self, context):
        scene = context.scene
        wm = context.window_manager

        # Get the sequence to edit the image
        media_sequence = get_media_sequence(scene.sequence_editor.active_strip)

        if not media_sequence:
            self.report({"ERROR"}, f"no clip on track: {scene.current_track}")
            return {"FINISHED"}

        #   Export frame and save
        # Set image format
        scene.render.image_settings.file_format = "JPEG"
        scene.render.image_settings.color_mode = "RGB"
        scene.render.image_settings.quality = 90
        scene.view_settings.view_transform = "Standard"

        # Hide annotations drawings
        context.space_data.show_annotation = False

        # Hide advanced drawings
        display_media_annotations(media_sequence, False)

        # Disable temporarily emphasize drawings
        keep_emphasize_drawings = wm.emphasize_drawings
        wm.emphasize_drawings = 0

        # Export
        image_path_base = utils_cache.get_temp_directory().joinpath("ext_edited")
        frame_current = scene.frame_current

        image_name = f"external_annotation_{frame_current}"
        image_path = image_path_base.joinpath(image_name).with_suffix(
            "." + scene.render.image_settings.file_format.lower()
        )

        scene.render.filepath = image_path.as_posix()
        bpy.ops.render.opengl(animation=False, sequencer=True, write_still=True)

        # Show annotations drawings
        context.space_data.show_annotation = True

        # Show advanced drawings
        display_media_annotations(media_sequence, True)

        # Restore emhasize drawings value
        wm.emphasize_drawings = keep_emphasize_drawings

        # Open image in external editing software
        bpy.ops.image.open(filepath=image_path.as_posix())
        bpy.ops.image.external_edit(filepath=image_path.as_posix())

        # Add IMAGE sequence targetting the exported image
        pending_note_sequence = get_active_pending_note_sequence(
            media_sequence, create=True
        )
        review_sequence = get_review_sequence(media_sequence)

        edited_image = pending_note_sequence.sequences.new_image(
            name=f"ext.{image_path.name}",
            filepath=image_path.as_posix(),
            frame_start=frame_current,
            channel=media_sequence.channel + len(pending_note_sequence.sequences),
        )
        edited_image.blend_type = "CROSS"
        edited_image["stax_kind"] = "externally_edited"

        # Update sequences size in timeline
        pending_note_sequence.update()
        review_sequence.update()

        # Refresh sequencer
        bpy.ops.sequencer.refresh_all()

        # Automatically start review
        context.scene.review_session_active = True

        # Set reviewed state to sequence
        set_reviewed_state(media_sequence)

        # Update image
        context.window_manager.modal_handler_add(self)
        return {"RUNNING_MODAL"}

    def modal(self, context, event):
        # Every time the user clicks, every externally edited images paths are reloaded to refresh edited image.
        # It could be optimized but we'll wait for users complain because the lack of reactivity is almost invisible.

        # Reload the image when clicking in the interface
        if event.type == "LEFTMOUSE":
            bpy.ops.sequencer.refresh_all()

        # Finish if the review session is stopped
        if not context.scene.review_session_active:
            return {"FINISHED"}

        return {"PASS_THROUGH"}


class WM_OT_start_advanced_drawing(MediaSequenceOperator, bpy.types.Operator):
    """Start review in advanced drawing mode"""

    bl_idname = "wm.start_advanced_drawing"
    bl_label = "Advanced Drawing"

    def execute(self, context):
        scene = context.scene
        data = bpy.data

        # Tell the impatient user to wait
        context.window.cursor_set("WAIT")

        # Get media sequence
        media_sequence = get_media_sequence(scene.sequence_editor.active_strip)

        # Set parameters
        media_path = get_source_media_path(media_sequence)
        gp_name = f"drawing.{media_path.name}"

        # Set source GP to main object
        data.objects["GPencil"].data = data.grease_pencils["GPencil"]

        # Switch to drawing scene
        drawing_scene = media_sequence.get("drawing_scene", None)
        if not drawing_scene:  # Create new drawing scene
            bpy.ops.scene.new(type="LINK_COPY")
            drawing_scene = context.scene
            drawing_scene.name = f"{media_path.name}_drawing"
            drawing_scene.sequence_editor_create()
            # NB: No need to set the new scene as active because scene.new() does it

            # Keep source scene and media sequence because
            drawing_scene["source_scene"] = scene
            drawing_scene["source_sequence"] = media_sequence.name

            # Add sound TODO replace by source scene strip when 2.93.8
            for seq in scene.sequence_editor.sequences_all:
                if (
                    seq.type == "SOUND"
                    and seq.frame_final_start >= media_sequence.frame_final_start
                    and seq.frame_final_end <= media_sequence.frame_final_end
                ):
                    sound_seq = drawing_scene.sequence_editor.sequences.new_sound(
                        name=seq.name,
                        filepath="",
                        channel=seq.channel,
                        frame_start=seq.frame_final_start,
                    )
                    sound_seq.sound = seq.sound
                    sound_seq.frame_final_end = seq.frame_final_end

        else:  # Set the existing drawing scene as active
            context.window.scene = drawing_scene

        # Switch to Drawing workspace
        context.window.workspace = data.workspaces["Advanced drawing"]

        # Set scene BG as transparent
        drawing_scene.render.film_transparent = True

        # #####
        # Set GPencil
        # #####

        # Activate autokey
        drawing_scene.tool_settings.use_keyframe_insert_auto = True

        # Get GP if sequence has one, else create one
        gp = media_sequence.get("gp", None)
        if gp is None:
            # Copy source GPencil
            gp = data.grease_pencils["GPencil"].copy()
            gp.name = gp_name
            # Set the new one to the GP object
            data.objects["GPencil"].data = gp

            # Associate it to the source sequence
            media_sequence["gp"] = gp
        else:
            # Set the associated GP to the GP object
            data.objects["GPencil"].data = gp

        # Settings
        # -------

        # Set vertex mode to FILL and STROKE
        drawing_scene.tool_settings.gpencil_paint.brush.gpencil_settings.vertex_mode = (
            "BOTH"
        )

        # Set pencil settings
        # Saving these data in the startup.blend doesn't work if the startup file
        # is loaded from the stax app template dir. Because it's a user configuration. TODO: is this a bug?
        drawing_scene.tool_settings.gpencil_paint.brush.gpencil_settings.pen_strength = (
            1.0
        )
        data.brushes["Pencil"].size = 8
        # -------

        # Empty Frame to start and end
        for layer in gp.layers:
            if not len(layer.frames):  # If not already created
                # -/+ 1 to have empty keyframes out of range to avoid user touching them
                # and drawings lasting from beginning
                layer.frames.new(media_sequence.frame_final_start - 1)
                layer.frames.new(media_sequence.frame_final_end + 1)

        # #####
        # Set current sequence as background camera clip
        # #####

        # Open the media as movie clip
        bpy.ops.clip.open(
            directory=str(media_path.parent),
            files=[{"name": media_path.name}],
            relative_path=False,
        )
        # Set to background of the camera
        # TODO: might have an issue if the clip has the same name of a previously used clip.
        camera = data.cameras["Camera"]
        bg_img = camera.background_images.values()[0]
        bg_img.clip = data.movieclips[media_path.name]

        # Fit the clip
        bg_img.frame_method = "FIT"
        # Offset clip time
        bg_img.clip.frame_start = media_sequence.frame_start
        # Set alpha to 1
        bg_img.alpha = (100 - context.window_manager.emphasize_drawings) / 100
        # Set depth
        bg_img.display_depth = "BACK"
        # ----------------------

        # Center the keyframes view
        # ---------------
        # Override context
        override = get_context("Advanced drawing", "GPENCIL")
        bpy.ops.action.view_all(override)

        # Set scene range to sequence
        drawing_scene.frame_start = media_sequence.frame_final_start
        drawing_scene.frame_end = media_sequence.frame_final_end

        # Update
        drawing_scene.frame_set(drawing_scene.frame_current)

        # Save
        bpy.ops.wm.save_mainfile()

        return {"FINISHED"}


class WM_OT_cancel_advanced_drawing(MediaSequenceOperator, bpy.types.Operator):
    """Cancel review in advanced drawing mode"""

    bl_idname = "wm.cancel_advanced_drawing"
    bl_label = "Advanced Drawing"

    def execute(self, context):
        scene = context.scene
        data = bpy.data
        wm = context.window_manager

        # Tell the impatient user to wait
        context.window.cursor_set("WAIT")

        # Get source scene
        source_scene = scene["source_scene"]

        # Get source media sequence
        media_sequence = source_scene.sequence_editor.sequences_all.get(
            scene["source_sequence"]
        )

        # Set parameters
        media_path = get_source_media_path(media_sequence)
        gp_name = f"drawing.{media_path.name}"

        # Set source GP to main object
        data.objects["GPencil"].data = data.grease_pencils["GPencil"]

        # If drawing scene associated to sequence, don't delete it,
        # it means it has been confirmed once and "cancel" musn't delete it.
        # In fact, only delete if no drawing scene has been associated to the reviewed sequence
        if not media_sequence.get("drawing_scene", None):
            # Delete created GP
            gp = data.grease_pencils.get(gp_name)
            data.grease_pencils.remove(gp)

            # Delete created scene
            data.scenes.remove(scene)
        else:  # Restore scene previous state of the grease pencil
            # There is a restore_gp_data linked to the drawing scene, it's copied and replaces the
            # current active one and is associated to the sequence as its main gp object
            old_gp_data = media_sequence.get("gp")

            # Copy the backup one as the main one
            restored_gp_data = scene.get("restore_gp_data").copy()

            # Remove the '#restore' extension in the name
            restored_gp_data.name = restored_gp_data.name.split("#")[0]

            # Set the restored to the grease pencil object
            context.active_object.data = restored_gp_data

            # Set the restored scene to the current sequence
            media_sequence["gp"] = restored_gp_data

            # Delete old drawing scene grease pencil data
            data.grease_pencils.remove(old_gp_data)

        # Exit mode
        exit_advanced_drawing_ui(context, source_scene)

        # Update emphasize drawings
        wm.emphasize_drawings = wm.emphasize_drawings

        # Save
        bpy.ops.wm.save_mainfile()

        return {"FINISHED"}


class WM_OT_complete_advanced_drawing(MediaSequenceOperator, bpy.types.Operator):
    """Complete review in advanced drawing mode"""

    bl_idname = "wm.complete_advanced_drawing"
    bl_label = "Advanced Drawing"

    def execute(self, context):
        scene = context.scene
        data = bpy.data
        wm = context.window_manager

        # Get source media sequence
        media_sequence = scene["source_scene"].sequence_editor.sequences_all.get(
            scene["source_sequence"]
        )

        # Set drawing scene to sequence to keep it
        media_sequence["drawing_scene"] = scene

        # #####
        # Render drawings
        # TODO when the GPencil from a scene strip is stable and fast enough, refactor this part
        # #####
        annotations_directory = Path(utils_cache.get_temp_directory(), "annotations")
        rendered_frames = []  # List of tuples (path_to_image, start, duration)

        # Clean previous rendered images references if needed
        pending_note_sequence = get_active_pending_note_sequence(
            media_sequence, create=True
        )
        for seq in pending_note_sequence.sequences:
            if seq.name.startswith("adv."):
                pending_note_sequence.sequences.remove(seq)

        # Set render settings TODO make it a function, duplicated from PublishSession
        scene.render.image_settings.file_format = "PNG"
        scene.render.image_settings.color_mode = "RGBA"
        scene.render.image_settings.compression = 70

        # Disable onion skins to avoid ghosts
        space_data = data.screens["Advanced drawing"].areas[1].spaces[0]
        space_data.overlay.use_gpencil_onion_skin = False

        # Hide BG white image
        camera = data.cameras["Camera"]
        camera.show_background_images = False

        # Hide background in viewport
        bg_empty = data.objects["Background"]
        bg_empty.hide_viewport = True

        # Disable temporarily emphasize drawings
        keep_emphasize_drawings = wm.emphasize_drawings
        wm.emphasize_drawings = 0

        #  Hide all layers
        gp = media_sequence.get("gp")
        for layer in gp.layers:
            layer.hide = True

        for layer in gp.layers:
            layer.hide = False  # Unhide layer to see it

            all_frames = list(layer.frames)
            for i, frame in enumerate(all_frames):
                if frame.frame_number < media_sequence.frame_final_end:
                    # Move playhead to frame
                    scene.frame_set(frame.frame_number)

                    # Calculate duration. If last GP frame use end frame
                    annotation_duration = (
                        all_frames[i + 1].frame_number - frame.frame_number
                    )

                    # Render only if frame has drawings
                    if len(frame.strokes):
                        # Build image name and path
                        annotation_name = f"{media_sequence.name}_{layer.info}_{frame.frame_number}_{str(uuid4())[:4]}"
                        annotation_path = annotations_directory.joinpath(
                            f"{annotation_name}.{scene.render.image_settings.file_format.lower()}"
                        )

                        # Set scene settings for render
                        scene.render.filepath = str(annotation_path)

                        # Export

                        # Override context if operator not run from Drawing workspace
                        bpy.ops.render.opengl(write_still=True)

                        # Keep the rendered image
                        rendered_frames.append(
                            (
                                annotation_path,
                                frame.frame_number,
                                annotation_duration,
                            )
                        )

            layer.hide = True  # Hide layer back

        # Show BG white image
        camera.show_background_images = True

        # Show back background
        bg_empty.hide_viewport = False

        # Restore emhasize drawings value
        wm.emphasize_drawings = keep_emphasize_drawings

        # Unhide all layers
        for layer in gp.layers:
            layer.hide = False

        # Set onion skins back
        space_data.overlay.use_gpencil_onion_skin = True

        # GP for restoring
        # --------
        current_gp_data = context.active_object.data

        restore_gp_data = scene.get("restore_gp_data", None)
        if restore_gp_data:  # Clean it if exists
            bpy.data.grease_pencils.remove(restore_gp_data)

        # Create a whole new one from the current one
        restore_gp_data = current_gp_data.copy()
        restore_gp_data.name = f"{current_gp_data.name}#restore"
        scene["restore_gp_data"] = restore_gp_data
        # ---------

        # #####
        # Back to SEQUENCER
        # #####

        # Set source GP to main object
        data.objects["GPencil"].data = data.grease_pencils["GPencil"]

        exit_advanced_drawing_ui(context, scene["source_scene"])

        # Add overlay to sequence
        # -----------------------

        # Clean existing overlay
        review_sequence = get_review_sequence(media_sequence)
        if review_sequence:
            for seq in review_sequence.sequences:
                if seq.name.startswith("drawings."):
                    review_sequence.sequences.remove(seq)

        if rendered_frames:
            # Create rendered frames as images sequences for overlay
            created_images = []
            for i, render in enumerate(rendered_frames):
                render_path, render_frame_start, render_duration = render
                image = pending_note_sequence.sequences.new_image(
                    name=f"adv.{render_path.name}",
                    filepath=str(render_path),
                    channel=pending_note_sequence.channel + i,
                    frame_start=render_frame_start,
                )
                if image:
                    image.frame_final_duration = render_duration
                    image.blend_type = "ALPHA_OVER"
                    image["stax_kind"] = "annotation"
                    image.pending = True
                    created_images.append(image)

            # Update pending note and review sequences for size in timeline
            pending_note_sequence.update()
            review_sequence.update()

        # Update emphasize drawings
        wm.emphasize_drawings = wm.emphasize_drawings

        # Consider review session as active
        context.scene.review_session_active = True

        # Set as reviewed
        set_reviewed_state(media_sequence)

        # Save
        bpy.ops.wm.save_mainfile()

        return {"FINISHED"}


class AddEmptyGPFrame(MediaSequenceOperator, bpy.types.Operator):
    """Add an empty frame for the current GPencil layer"""

    bl_idname = "scene.add_empty_gp_frame"
    bl_label = "Empty frame"

    def execute(self, context):
        scene = context.scene

        # Select the active gpencil
        if scene is bpy.data.scenes["Scene"]:
            gp = bpy.data.grease_pencils["Annotations"]
        else:
            gp = context.gpencil_data

        grease_pencil_layer_frames = gp.layers.active.frames

        # Test if there is already a frame and delete it
        for frame in grease_pencil_layer_frames:
            if frame.frame_number == context.scene.frame_current:
                grease_pencil_layer_frames.remove(frame)

        # Create keyframe for every layer
        frame = grease_pencil_layer_frames.new(context.scene.frame_current)

        # Hack to refresh the drawing... TODO report that to Blender
        scene.fill_opacity = scene.fill_opacity
        return {"FINISHED"}


class SCENE_OT_clear_pending_note(bpy.types.Operator):
    """Clear pending note of the current displayed sequence"""

    bl_idname = "scene.clear_pending_note"
    bl_label = "Clear Pending Note"

    @classmethod
    def poll(cls, context):
        media_sequence = get_media_sequence(context.scene.sequence_editor.active_strip)

        if media_sequence and bpy.data.scenes["Scene"].review_session_active:
            # Test if at least one annotate drawing is over the sequence
            for layer in bpy.data.grease_pencils["Annotations"].layers:
                for frame in layer.frames:
                    if (
                        media_sequence.frame_start
                        <= frame.frame_number
                        < media_sequence.frame_final_end
                        and frame.strokes
                    ):
                        return True

            # Test if rendered drawing images are associated to sequence
            return get_pending_notes_sequences(media_sequence) is not []

    def execute(self, context):
        sequence_editor = context.scene.sequence_editor
        data = bpy.data
        media_sequence = get_media_sequence(sequence_editor.active_strip)

        bpy.ops.sequencer.select_all(action="DESELECT")

        clear_media_reviews([media_sequence], pending_note_only=True)

        # Create the empty drawing at start back to stop drawings at each shot
        if media_sequence.frame_start not in [
            f.frame_number
            for f in data.grease_pencils["Annotations"].layers["Note"].frames
        ]:
            data.grease_pencils["Annotations"].layers["Note"].frames.new(
                media_sequence.frame_start
            )

        return {"FINISHED"}


class WM_OT_stax_layer_annotation_add(bpy.types.Operator):
    """Enhances the native gpencil.layer_annotation_add to add empty keyframes to every start/end of medias"""

    bl_idname = "gpencil.stax_layer_annotation_add"
    bl_label = "Add Annotation Layer"

    @classmethod
    def poll(cls, context):
        if context.scene.sequence_editor.sequences:
            return True

    def execute(self, context):
        bpy.ops.gpencil.layer_annotation_add()

        # Add empty keyframes to all medias boundaries of the current layer
        gpd = context.annotation_data
        created_layer = gpd.layers[-1]

        all_sequences = context.scene.sequence_editor.sequences
        last_sequence = all_sequences[-1]
        cut_keyframes = []  # Keep the created keyframes to avoid error
        for seq in all_sequences:
            if seq.frame_start not in cut_keyframes:
                created_layer.frames.new(seq.frame_start)
            cut_keyframes.append(seq.frame_start)

            # Get the latest sequence horizontally
            if seq.frame_final_end > last_sequence.frame_final_end:
                last_sequence = seq

        # Add the last empty frame
        created_layer.frames.new(last_sequence.frame_final_end + 1)

        return {"FINISHED"}


class WM_OT_stax_layer_add(bpy.types.Operator):
    """Enhances the native gpencil.layer_add to add empty keyframes to the current reviewed media"""

    bl_idname = "gpencil.stax_layer_add"
    bl_label = "Add Layer"

    @classmethod
    def poll(cls, context):
        if context.scene is not bpy.data.scenes["Scene"]:
            return True

    def execute(self, context):
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        gpd = context.gpencil_data

        # Create new layer
        bpy.ops.gpencil.layer_add()
        created_layer = gpd.layers[-1]

        # Add empty keyframes to all medias boundaries of the current layer
        created_layer.frames.new(current_sequence.frame_start - 1)
        created_layer.frames.new(current_sequence.frame_final_end + 1)

        return {"FINISHED"}


classes = [
    WM_OT_SelectExternalImageEditor,
    WM_OT_edit_image_externally,
    WM_OT_start_advanced_drawing,
    WM_OT_cancel_advanced_drawing,
    WM_OT_complete_advanced_drawing,
    AddEmptyGPFrame,
    SCENE_OT_clear_pending_note,
    WM_OT_stax_layer_annotation_add,
    WM_OT_stax_layer_add,
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
