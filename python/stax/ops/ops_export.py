# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to the export tool
"""

import bpy
from math import sin, cos, pi


class NewRenderTrack(bpy.types.Operator):
    """Add a new track to be rendered"""

    bl_idname = "scene.new_render_track"
    bl_label = "Add new render track"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        scene = context.scene

        # Add track to render tracks
        new_track = scene.render_tracks.add()
        new_track.channel = len(scene.render_tracks)

        return {"FINISHED"}


class RemoveRenderTrack(bpy.types.Operator):
    """Remove a track in render tracks list"""

    bl_idname = "scene.remove_render_track"
    bl_label = "Remove render track"
    bl_options = {"REGISTER", "UNDO"}

    name: bpy.props.StringProperty(name="Track name", subtype="FILE_NAME")
    channel: bpy.props.IntProperty(
        name="Channel to export",
        description="To be used when track has no name",
    )

    def execute(self, context):
        scene = context.scene

        # Find track index from name
        track_index = scene.render_tracks.find(self.name)

        # Remove track
        scene.render_tracks.remove(track_index)

        return {"FINISHED"}


class ExportRender(bpy.types.Operator):
    """Render"""

    bl_idname = "scene.export_render"
    bl_label = "Render sequence"

    def execute(self, context):
        scene = context.scene
        seq_ed = context.scene.sequence_editor
        hidden_sequences = []

        # Display loading for user
        context.window.cursor_set("WAIT")

        #       Create strips
        #   Sound, if a track 'Sound' is found, it'll be automatically added
        sound_index = max(scene.tracks.find("Sound"), scene.tracks.find("sound"))
        if sound_index >= 0:
            sound_channel = sound_index + 1
            sound_sequences = [
                s for s in seq_ed.sequences if s.channel == sound_channel
            ]

            # Create track meta
            meta_sound = seq_ed.sequences.new_meta(
                f"track.sound", sound_channel, scene.frame_start
            )

            # Sound meta
            meta_sound["stax_kind"] = "track"
            meta_sound.blend_type = "ALPHA_OVER"

            # Move to meta
            for sound in sound_sequences:
                sound.move_to_meta(meta_sound)

        #    Movies
        rotation = 2 * pi / len(scene.render_tracks)

        for i, render_track in enumerate(scene.render_tracks, 1):
            # Create track meta
            track_meta = seq_ed.sequences.new_meta(
                f"track.{render_track.name}", render_track.channel, scene.frame_start
            )
            track_meta["stax_kind"] = "track"

            # Create vignette meta sequence, to move all movies to the correct place
            vignette_meta = track_meta.sequences.new_meta(
                f"vignette.{render_track.name}", render_track.channel, scene.frame_start
            )
            vignette_meta["stax_kind"] = "vignette"

            # Move to meta track and mute if shot is not displayed
            for seq in seq_ed.sequences:
                # Mute
                shot = render_track.shots.get(seq.name)
                if shot and not shot.display:
                    seq.mute = True
                    hidden_sequences.append(seq)

                # Move
                if seq.channel == render_track.channel and seq != track_meta:
                    seq.move_to_meta(vignette_meta)

            # Add transform strip and set parameters
            transform = track_meta.sequences.new_effect(
                name="",
                type="TRANSFORM",
                channel=track_meta.channel + 1,
                frame_start=0,
                seq1=vignette_meta,
            )
            transform.blend_type = "ALPHA_OVER"
            transform.use_uniform_scale = True
            transform["stax_kind"] = "transform"

            # Tracks meta strip transform
            if len(scene.render_tracks) > 1:
                transform.scale_start_x = 0.5 - 0.05
                if len(scene.render_tracks) == 2:
                    translate_x_sign = 1 if cos(rotation * -i) < 0 else -1
                    transform.translate_start_x = translate_x_sign * 25
                    transform.translate_start_y = 0
                elif len(scene.render_tracks) == 3:
                    translate_x_sign = 1 if cos(rotation * -i) < 0 else -1
                    translate_y_sign = 1 if sin(rotation * -i) < 0 else -1
                    transform.translate_start_x = translate_x_sign * 25
                    transform.translate_start_y = translate_y_sign * 25 if i != 0 else 0
                elif len(scene.render_tracks) == 4:
                    translate_x_sign = 1 if cos(rotation * -i) < 0 else -1
                    translate_y_sign = 1 if sin(rotation * -i) < 0 else -1
                    transform.translate_start_x = translate_x_sign * 25
                    transform.translate_start_y = translate_y_sign * 25
            else:
                transform.scale_start_x = 1

            # Add text strip
            text = track_meta.sequences.new_effect(
                name="",
                type="TEXT",
                channel=transform.channel + 1,
                frame_start=0,
                frame_end=scene.frame_end,
            )
            text.text = "".join(
                [
                    render_track.name,
                    " -- " if render_track.comment else "",
                    render_track.comment,
                ]
            )
            text.font_size = 20
            text.align_y = "TOP"
            text.location = [
                0.5 + transform.translate_start_x / 100,
                0.05
                if len(scene.render_tracks) == 1
                else 0.28 + transform.translate_start_y / 100,
            ]
            text.blend_type = "ALPHA_OVER"
            text["stax_kind"] = "text_label"

            # Track blend type
            track_meta.blend_type = "ALPHA_OVER"

        # Hide other sequences
        for seq in seq_ed.sequences:
            if seq.get("stax_kind") != "track" and not seq.mute:
                seq.mute = True
                hidden_sequences.append(seq)

        # Start render
        bpy.ops.render.render(animation=True)

        # Unhide sequences
        for seq in hidden_sequences:
            seq.mute = False

        # Clean sequencer and reload tracks
        self.clean_timeline(context)

        self.report({"INFO"}, "Export render finished")

        return {"FINISHED"}

    @staticmethod
    def clean_timeline(context):
        """Clean the timeline and set back tracks"""
        seq_ed = context.scene.sequence_editor

        # Clear track meta
        for sequence in seq_ed.sequences:
            if sequence.get("stax_kind") == "track":
                seq_ed.active_strip = sequence  # TODO refactor when .separate() https://developer.blender.org/D11995

                # Clear text
                for seq in sequence.sequences:
                    if seq.get("stax_kind") in ["text_label", "transform"]:
                        sequence.sequences.remove(seq)

                # Separate
                bpy.ops.sequencer.meta_separate()

        # Clear label meta
        for sequence in seq_ed.sequences:
            if sequence.get("stax_kind") == "vignette":
                seq_ed.active_strip = sequence  # TODO refactor when .separate() https://developer.blender.org/D11995

                # Separate
                bpy.ops.sequencer.meta_separate()


classes = [NewRenderTrack, RemoveRenderTrack, ExportRender]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
