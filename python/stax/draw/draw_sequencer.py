# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


# Some Code from Blender Kitsu addon, Created by: Sybren A. Stuevel, modified by: Paul Golter
# Current Stax version made by: Félix David

"""
Sequencer Draw
Additional drawing into the sequencer
"""

from collections import namedtuple
import typing
from typing import List, Sequence, Tuple

import bpy
from bpy.types import MetaSequence
import blf
import bgl
import gpu
from gpu_extras.batch import batch_for_shader

from stax.utils.utils_timeline import get_media_sequence, get_reviewed_media_sequences

FakeSequence = namedtuple(
    "FakeSequence",
    ["channel", "frame_final_start", "frame_final_end", "frame_final_duration"],
)


def draw_sequencer():
    """Draw sequencer OpenGL"""
    context = bpy.context
    scene = context.scene
    sequence_editor = scene.sequence_editor
    # Sentinel if no sequences, abort
    if not hasattr(bpy.context.scene.sequence_editor, "sequences"):
        return

    region = bpy.context.region

    # TODO make it homogeneous

    # Defining boundaries of the area to crop rectangles in view pixels' space
    x_view_min, y_view_min = region.view2d.region_to_view(0, 11)
    x_view_max, y_view_max = region.view2d.region_to_view(
        region.width - 11, region.height - 22
    )
    view_boundaries = (x_view_min, y_view_min, x_view_max, y_view_max)

    # Draw reviewed symbol
    # Workaround to avoid slowing from opengl draw over when the animation is playing
    if not context.screen.is_animation_playing:
        draw_reviewed(x_view_min, y_view_min, x_view_max, y_view_max)

    # Do not display track names if toggled meta
    if hasattr(scene, "tracks") and not sequence_editor.meta_stack:
        draw_channel_name(view_boundaries)


def draw_channel_name(view_boundaries):
    """Draw tracks name

    :param view_boundaries: View boundaries
    """
    region = bpy.context.region
    font_id = 0

    x_view_fixed, y_view_fixed = region.view2d.region_to_view(20, 50)
    width_track_frame_fixed = 150

    shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
    shader.bind()

    # Get tracks
    tracks = bpy.context.scene.tracks

    # Draw track rectangles
    bgl.glEnable(bgl.GL_BLEND)  # Enables alpha channel
    dark_track = False
    for i in range(
        0, len(tracks)
    ):  # Out of text loop because of OpenGL issue, rectangles are bugging
        shader.uniform_float("color", (0.66, 0.66, 0.66, 0.7))

        # Alternate track color
        if dark_track:
            shader.uniform_float("color", (0.5, 0.5, 0.5, 0.7))
        dark_track = not dark_track

        # Build track rectangle
        track_rectangle = get_rectangle_region_coordinates(
            (x_view_fixed, i + 1, x_view_fixed, i + 2), view_boundaries
        )

        vertices, indices = build_rectangle(
            (
                track_rectangle[0],
                track_rectangle[1],
                track_rectangle[2] + width_track_frame_fixed,
                track_rectangle[3],
            ),
            0,
        )

        # Draw the rectangle
        batch = batch_for_shader(shader, "TRIS", {"pos": vertices}, indices=indices)
        batch.draw(shader)

    # Display text
    i = 0
    for track in tracks:
        x, y = region.view2d.view_to_region(x_view_fixed, i + 1.4)

        blf.position(font_id, 30, y, 0)
        blf.size(font_id, 14, 72)
        blf.color(font_id, 1, 1, 1, 0.9)
        blf.draw(font_id, track.name)

        i += 1


def get_rectangle_region_coordinates(
    rectangle=(0, 0, 5, 5), view_boundaries=(0, 0, 1920, 1080)
):
    """Calculates strip coordinates in region's pixels norm, clamped into region

    :param rectangle: Rectangle to get coordinates of
    :param view_boundaries: Cropping area
    :return: Strip coordinates in region's pixels norm, resized for status color bar
    """
    region = bpy.context.region

    # Converts x and y in terms of the grid's frames and channels to region pixels coordinates clamped
    x1, y1 = region.view2d.view_to_region(
        max(view_boundaries[0], min(rectangle[0], view_boundaries[2])),
        max(view_boundaries[1], min(rectangle[1], view_boundaries[3])),
    )
    x2, y2 = region.view2d.view_to_region(
        max(view_boundaries[0], min(rectangle[2], view_boundaries[2])),
        max(view_boundaries[1], min(rectangle[3], view_boundaries[3])),
    )

    return x1, y1, x2, y2


def build_rectangle(coordinates, indices_offset):
    """Build an OpenGL rectangle into region

    :param coordinates: Four points coordinates
    :param indices_offset: Rectangle's indices offset
    :return: Built vertices and indices
    """
    # Building the Rectangle
    vertices = (
        (coordinates[0], coordinates[1]),
        (coordinates[2], coordinates[1]),
        (coordinates[0], coordinates[3]),
        (coordinates[2], coordinates[3]),
    )

    indices = (
        (0 + indices_offset, 1 + indices_offset, 2 + indices_offset),
        (2 + indices_offset, 1 + indices_offset, 3 + indices_offset),
    )

    return vertices, indices


def build_rectangle_raw(coordinates):
    """Build an OpenGL rectangle into region

    :param coordinates: Four points coordinates
    :param indices_offset: Rectangle's indices offset
    :return: Built vertices and indices
    """
    vertices = (
        (coordinates[0], coordinates[1]),
        (coordinates[2], coordinates[1]),
        (coordinates[0], coordinates[3]),
        (coordinates[2], coordinates[3]),
    )

    return vertices[0], vertices[1], vertices[3], vertices[0], vertices[3], vertices[2]


# glsl
gpu_vertex_shader = """
uniform mat4 ModelViewProjectionMatrix;

layout (location = 0) in vec3 pos;
layout (location = 1) in vec4 color;

out vec4 lineColor; // output to the fragment shader

void main()
{
    gl_Position = ModelViewProjectionMatrix * vec4(pos.x, pos.y, 0.0, 1.0);
    lineColor = color;
}
"""

gpu_fragment_shader = """
out vec4 fragColor;
in vec4 lineColor;

void main()
{
    fragColor = lineColor;
}
"""

Float2 = typing.Tuple[float, float]
Float3 = typing.Tuple[float, float, float]
Float4 = typing.Tuple[float, float, float, float]
LINE_WIDTH = 10


# Get status color
status_colors = {
    "waiting review": [1, 0.82, 0.16],
    "approved": [0.18, 0.86, 0.37],
    "rejected": [0.91, 0.1, 0.1],
}


class RectDrawer:
    def __init__(self):
        self._format = gpu.types.GPUVertFormat()
        self._pos_id = self._format.attr_add(
            id="pos", comp_type="F32", len=2, fetch_mode="FLOAT"
        )
        self._color_id = self._format.attr_add(
            id="color", comp_type="F32", len=4, fetch_mode="FLOAT"
        )

        self.shader = gpu.types.GPUShader(gpu_vertex_shader, gpu_fragment_shader)

    def draw(self, coords: typing.List[Float3], colors: typing.List[Float4]):
        global LINE_WIDTH

        if not coords:
            return

        bgl.glEnable(bgl.GL_BLEND)
        bgl.glLineWidth(LINE_WIDTH)

        vbo = gpu.types.GPUVertBuf(len=len(coords), format=self._format)
        vbo.attr_fill(id=self._pos_id, data=coords)
        vbo.attr_fill(id=self._color_id, data=colors)

        batch = gpu.types.GPUBatch(type="TRIS", buf=vbo)
        batch.program_set(self.shader)
        batch.draw()


def get_strip_status_rect(strip, status_rect_height=1) -> Float4:

    # Get x and y in terms of the grid's frames and channels
    x1 = strip.frame_final_start
    x2 = strip.frame_final_end
    # seems to be a 5 % offset from channel top start of strip
    y1 = strip.channel + 0.05
    y2 = strip.channel - 0.05 + status_rect_height

    return x1, y1, x2, y2


def draw_sequences_overlay(line_drawer: RectDrawer):

    # Workaround to avoid slowing from opengl draw over when the animation is playing
    if bpy.context.screen.is_animation_playing:
        return

    global LINE_WIDTH

    context = bpy.context
    sequence_editor = context.scene.sequence_editor

    if not sequence_editor:
        return

    region = context.region

    # Draw status depending on level of meta stack
    sequences_stack = (
        sequence_editor.meta_stack[-1].sequences
        if sequence_editor.meta_stack
        else sequence_editor.sequences
    )
    coords = []  # type: typing.List[Float2]
    colors = []  # type: typing.List[Float4]
    region_rectangle = (
        region.view2d.region_to_view(0, 0),
        region.view2d.region_to_view(region.width, region.height),
    )

    # Build statuses
    build_sequence_overlays(region, sequences_stack, coords, colors, region_rectangle)

    line_drawer.draw(coords, colors)


def build_sequence_overlays(
    region,
    sequences_stack: List[Sequence],
    coords: List,
    colors: List,
    region_rectangle: List[Tuple[int]],
    main_meta: MetaSequence = None,
):
    """Build sequence overlays from sequence stack.

    Display over the meta sequence:
        - Statuses of media sequences
        - Images annotations markers

    :param region: UI Region to draw
    :param sequences_stack: Stack containing the media sequences
    :param coords: List of coords of rectangles to draw
    :param colors: List of colors of rectangles
    :param main_meta: Main meta in VSE, optional
    """
    user_preferences = bpy.context.scene.user_preferences
    reviewed_sequences = get_reviewed_media_sequences()

    # Collect all the lines (vertex coords + vertex colours) to draw.
    for sequence in sequences_stack:

        rectangle_and_color = []

        # check if any of the coordinates are out of bounds
        displayed_sequence = main_meta or sequence
        if (
            displayed_sequence.frame_final_start > region_rectangle[1][0]  # xwin2
            or displayed_sequence.frame_final_end < region_rectangle[0][0]  # xwin1
            or displayed_sequence.channel + 0.05 > region_rectangle[1][1]  # ywin2
            or displayed_sequence.channel - 0.05 < region_rectangle[0][1]  # ywin1
        ):
            continue

        # Don't draw for non media sequences
        if sequence.type not in ["MOVIE", "SOUND", "IMAGE", "META"]:
            continue
        elif sequence.type == "META":
            # Use main meta or current sequence if there is None
            build_sequence_overlays(
                region,
                sequence.sequences,
                coords,
                colors,
                region_rectangle,
                main_meta=main_meta if main_meta else sequence,
            )
        # Image annotation marker
        elif (
            sequence.get("stax_kind") in ["temp", "annotation", "externally_edited"]
            and sequence.frame_final_end > displayed_sequence.frame_final_start
            and sequence.frame_final_start < displayed_sequence.frame_final_end
        ):
            is_pending = sequence.pending or sequence["stax_kind"] != "annotation"
            rectangle_and_color.append(
                build_sequence_annotation_markers(
                    sequence,
                    (
                        user_preferences.pending_image_annotation_marker_color
                        if is_pending
                        else user_preferences.image_annotation_marker_color
                    ),
                    main_meta,
                    is_pending=is_pending,
                )
            )
        elif sequence.get("stax_kind") == "note":
            continue
        else:  # Media sequence
            # Annotate tool annotations
            if sequence in reviewed_sequences:
                gp = bpy.data.grease_pencils["Annotations"]
                for layer in gp.layers:
                    layer.hide = False  # Unhide layer to see it

                    # Hide onion skin
                    layer.use_annotation_onion_skinning = False

                    # Get frames in the sequence range if the frame
                    sequence_frames = [
                        frame
                        for frame in layer.frames
                        if (
                            displayed_sequence.frame_start
                            <= frame.frame_number
                            < displayed_sequence.frame_final_end
                        )
                    ]
                    for i, frame in enumerate(sequence_frames):
                        if not len(frame.strokes):  # Sentinel
                            continue

                        # Calculate duration. If last GP frame use end frame of sequence
                        if frame is sequence_frames[-1]:
                            annotation_duration = (
                                displayed_sequence.frame_final_end - frame.frame_number
                            )
                        else:
                            annotation_duration = (
                                sequence_frames[i + 1].frame_number - frame.frame_number
                            )

                        rectangle_and_color.append(
                            build_sequence_annotation_markers(
                                FakeSequence(
                                    displayed_sequence.channel,
                                    frame.frame_number,
                                    frame.frame_number + annotation_duration,
                                    annotation_duration,
                                ),
                                user_preferences.pending_image_annotation_marker_color,
                                main_meta,
                                is_pending=True,
                            )
                        )

            # Status
            # Get color
            color = tuple(
                status_colors.get(
                    sequence.get(
                        "pending_status",
                    )
                    or sequence.get("current_status", "waiting review")
                )
            )

            # Draw status
            rectangle_and_color.append(
                build_sequence_status(sequence, color, main_meta)
            )

        # Add shape and color to list
        for rect_and_col in rectangle_and_color:
            coords.extend(rect_and_col[0])
            colors.extend(rect_and_col[1])


def build_sequence_status(
    sequence: Sequence,
    color: Tuple[float, float, float, float],
    main_meta: MetaSequence = None,
):
    """Build sequence status color label.

    :param sequence: Sequence to draw over
    :param color: Drawing color of markers
    :param main_meta: Main meta in VSE, optional
    """

    alpha = 1

    # Set status' rectangle height
    status_rect_height = 0.25

    if main_meta:
        # Custom use sequence's boundaries if smaller than main meta's
        strip_coords = (
            max(sequence.frame_final_start, main_meta.frame_final_start),
            main_meta.channel + 0.05,
            min(sequence.frame_final_end, main_meta.frame_final_end),
            main_meta.channel - 0.05 + status_rect_height,
        )
    else:
        strip_coords = get_strip_status_rect(sequence, status_rect_height)

    return build_rectangle_raw(strip_coords), [color + (alpha,)] * 6


def build_sequence_annotation_markers(
    sequence: Sequence,
    color: Tuple[float, float, float, float],
    main_meta: MetaSequence = None,
    is_pending=False,
):
    """Build sequence image annotation markers.

    Pending markers are taller and a bit transparent.

    :param sequence: Sequence to draw over
    :param color: Drawing color of markers
    :param main_meta: Main meta in VSE, optional
    :param is_pending: Pending annotation for different marker aspect, optional
    """
    annotation_coords_list = []

    displayed_sequence = main_meta or sequence

    # Custom use sequence's boundaries if smaller than main meta's
    base_x = max(sequence.frame_final_start, displayed_sequence.frame_final_start)
    pending_offset_y = 0.1 if is_pending else 0
    base_y = displayed_sequence.channel + 0.20
    top_y = base_y + 0.30 + pending_offset_y
    duration_bar_thickness = 0.2 if is_pending else 0.1
    alpha = 1

    if sequence.frame_final_start >= displayed_sequence.frame_final_start:
        frame_coords = (
            # Bottom left corner
            base_x,
            base_y,
            # Top right corner
            base_x + 1,
            top_y,
        )
        rectangle_coords = build_rectangle_raw(frame_coords)

        # Build spike on top
        spike_coords = (
            (sequence.frame_final_start, top_y),
            (sequence.frame_final_start + 0.5, top_y + 0.1),
            (sequence.frame_final_start + 1, top_y),
        )

        annotation_coords_list.extend(rectangle_coords + spike_coords)

    if sequence.frame_final_duration > 1:  # Annotation has duration
        duration_coords = (
            # Bottom left corner
            max(sequence.frame_final_start, displayed_sequence.frame_final_start) + 1,
            base_y,
            # Top right corner
            min(sequence.frame_final_end, displayed_sequence.frame_final_end),
            base_y + duration_bar_thickness,  # Bar thickness
        )
        annotation_coords_list.extend(build_rectangle_raw(duration_coords))

    return annotation_coords_list, [tuple(color) + (alpha,)] * len(
        annotation_coords_list
    )


def draw_reviewed(x_view_min, y_view_min, x_view_max, y_view_max):
    """Draw sequencer blf"""
    sequence_editor = bpy.context.scene.sequence_editor
    region = bpy.context.region

    # Draw status depending on level of meta stack
    for sequence in sequence_editor.sequences:
        media_sequence = get_media_sequence(sequence)

        if not media_sequence:  # Sentinel
            continue

        if media_sequence.get("reviewed"):

            # If sequence out of bounds
            if (
                x_view_max < sequence.frame_final_start
                or x_view_min > sequence.frame_final_end - 2
                or y_view_min > sequence.channel
                or y_view_max < sequence.channel - 1
            ):
                continue

            # Clamp to region size and convert to region size
            x1, y1 = region.view2d.view_to_region(
                max(x_view_min + 2, min(sequence.frame_final_start, x_view_max)),
                sequence.channel,
            )
            x2, y2 = region.view2d.view_to_region(
                max(x_view_min, min(sequence.frame_final_end, x_view_max)),
                sequence.channel + 1,
            )

            blf.position(0, x1, y1, 0)

            reviewed_symbol_height = int(y2 - y1)
            # Draw only if horizontal size is greater than a value
            if x2 - x1 > reviewed_symbol_height / 2:
                blf.size(0, min(reviewed_symbol_height, 60), 72)  # Size clamped
                blf.color(0, 1, 1, 1, 0.8)
                blf.draw(0, "*")


def tag_redraw_all_sequencer_editors():
    context = bpy.context

    # Py cant access notifiers
    for window in context.window_manager.windows:
        for area in window.screen.areas:
            if area.type == "SEQUENCE_EDITOR":
                for region in area.regions:
                    if region.type == "WINDOW":
                        region.tag_redraw()


# This is a list so it can be changed instead of set
# if it is only changed, it does not have to be declared as a global everywhere
cb_handle = []


def callback_enable():
    global cb_handle

    if cb_handle:
        return

    # Doing GPU stuff in the background crashes Blender, so let's not.
    if bpy.app.background:
        return

    status_drawer = RectDrawer()
    cb_handle[:] = (
        bpy.types.SpaceSequenceEditor.draw_handler_add(
            draw_sequences_overlay, (status_drawer,), "WINDOW", "POST_VIEW"
        ),  # TODO two callbacks needed?
        bpy.types.SpaceSequenceEditor.draw_handler_add(
            draw_sequencer, (), "WINDOW", "POST_PIXEL"
        ),
    )

    tag_redraw_all_sequencer_editors()


def callback_disable():
    global cb_handle

    if not cb_handle:
        return

    try:
        bpy.types.SpaceSequenceEditor.draw_handler_remove(cb_handle[0], "WINDOW")
    except ValueError:
        # Thrown when already removed.
        pass
    cb_handle.clear()

    tag_redraw_all_sequencer_editors()


# ---------REGISTER ----------


def register():
    callback_enable()


def unregister():
    callback_disable()
