# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
All functions to perform reviews actions shared by several operators
"""

from datetime import datetime
from hashlib import md5
from pathlib import Path
from typing import List, Set, Tuple
from uuid import uuid4

import bpy
from bpy.types import GPencilLayer, MetaSequence, GPencilFrame
import openreviewio as orio
from openreviewio import ImageAnnotation, MediaReview, Note, Status

from stax.utils.utils_core import get_text_body
from stax.utils.utils_cache import get_temp_directory
from stax.utils.utils_timeline import (
    build_substitute,
    get_media_sequence,
    get_metas_hierarchy,
    get_holding_stack,
    get_review_sequence,
    get_reviewable_media_sequences,
    get_reviewed_media_sequences,
    get_source_media_path,
    MediaSequence,
)


def available_statuses_callback(screen, context):
    """Get available statuses as items for enum property.

    :param screen: Blender current screen object
    :param context: Blender context
    """
    return [
        (status, status.capitalize(), "No description")
        for status in reversed(orio.allowed_statuses)
    ]


def build_pending_notes() -> List[Tuple[MediaSequence, MediaReview, Note]]:
    """Build OpenReviewIO notes (and eventually Reviews) based on pending notes (from timeline and texts).

    :return: List of media sequences with their reviews and the created notes as `[(MediaSequence, MediaReview, Note), ...]`
    """
    scene = bpy.context.scene
    gp = bpy.data.grease_pencils["Annotations"]
    annotations_directory = Path(get_temp_directory(), "annotations")
    current_frame = scene.frame_current
    author = scene.user_preferences.author_name

    # Set render settings
    scene.render.image_settings.file_format = "PNG"
    scene.render.image_settings.color_mode = "RGBA"
    scene.render.image_settings.compression = 70

    # Hide all media sequences to render only Annotations
    reviewable_sequences = get_reviewable_media_sequences()
    for sequence in reviewable_sequences:
        sequence.mute = True

    # OpenReviewIO building
    edited_notes = []  # (MediaSequence, MediaReview, Note) list to be returned

    # Image annotations, text comments and statuses
    # ===================
    #  Hide all layers
    for layer in gp.layers:
        layer.hide = True

    #  Render for all frames with drawings
    stax_review = None

    # To store {Frame number: (related sequence, ImageAnnotation)}
    # This keeps reference frames and avoid duplicates, to render only one reference by drawn frame
    reference_frames = {}

    reviewed_sequences = get_reviewed_media_sequences()
    for media_sequence in reviewed_sequences:

        # Get review sequence
        review_sequence = get_review_sequence(media_sequence)

        # Get sequence in main timeline
        metas_hierarchy = get_metas_hierarchy(media_sequence)
        visible_sequence = (
            metas_hierarchy[1] if len(metas_hierarchy) > 1 else media_sequence
        )

        # New/Get stax review and new note of current sequence
        # =======================================

        # Create default review if None already exists
        media_path = get_source_media_path(media_sequence)
        stax_reviews = extract_media_reviews([review_sequence])
        if stax_reviews:
            stax_review = stax_reviews[0]
        else:
            stax_review = MediaReview(get_source_media_path(media_sequence))

        # Set media status to review
        stax_review.status = media_sequence.get("pending_status") or "waiting review"

        # TODO iterate all ORIO attributes?
        if review_sequence:
            stax_review.metadata = review_sequence.get("metadata", {})

        # Dynamic function
        # -------------------
        def create_image_annotation(
            related_sequence, orio_note, start_frame, duration, image_path
        ):
            """Create ORIO ImageAnnotation, add it to note and keep the reference for later rendering"""
            # Offset annotation start frame if video is offset, to be sure to be in media time
            annotation_frame = start_frame - related_sequence.frame_start
            content = orio.Content.ImageAnnotation(
                frame=annotation_frame,
                duration=duration,
                path_to_image=image_path,
            )
            orio_note.add_content(content)

            # Reference Image
            if reference_frames.get((start_frame, related_sequence)):
                reference_frames[(start_frame, related_sequence)].append(content)
            else:
                reference_frames[(start_frame, related_sequence)] = [content]

        # -------------

        # Create note
        active_pending_note_sequence = get_active_pending_note_sequence(
            media_sequence, create=True
        )
        pending_replies_sequences = [
            seq
            for seq in get_pending_notes_sequences(media_sequence)
            if seq.get("parent")
        ]

        # Iterate all pending notes
        # orio_note will remain the one of the active pending note sequence and annotate drawings will be added to it
        for pending_note_sequence in pending_replies_sequences + [
            active_pending_note_sequence
        ]:
            orio_note = orio.Note(author=author)

            # Get parent note if exists
            parent_note = pending_note_sequence.get("parent")
            if parent_note:
                orio_note.parent = stax_review.get_note(parent_note)

            # #####
            # Add Contents:
            #  - OpenGL rendered images
            #  - Text comments and annotations
            # #####

            # Render for all rendered frames
            for image_sequence in pending_note_sequence.sequences:
                # Create ORIO content and add it to note
                create_image_annotation(
                    media_sequence,
                    orio_note,
                    image_sequence.frame_start,
                    image_sequence.frame_final_duration,
                    get_source_media_path(image_sequence),
                )

            # Create text contents
            # ====================
            contents = pending_note_sequence.get("contents", {})

            # Create Text comment
            # -------------------
            for text_comment in contents.get("TextComment", []):
                # Get text content
                body = get_text_body(text_comment)

                # Add text comment to note
                content = orio.Content.TextComment(body=body)
                orio_note.add_content(content)

            # Create Text annotations
            # -----------------------
            for annotation in contents.get("TextAnnotation", []):
                # Get text content
                body = get_text_body(annotation)

                # Add text comment to note
                start_frame = annotation.get("frame_start")
                content = orio.Content.TextAnnotation(
                    body=body,
                    frame=start_frame,
                    duration=max(annotation.get("frame_end") - start_frame, 1),
                )
                orio_note.add_content(content)

            # Remove the text contents from the sequence
            pending_note_sequence.pop("contents", None)

            # Add the replies only if has contents, because they aren't added later
            if orio_note.parent:
                edited_notes.append(
                    (
                        media_sequence,
                        stax_review,
                        orio_note if orio_note.contents else None,
                    )
                )

        # Annotations
        # ===========
        # Hide main sequence
        visible_sequence.mute = True

        # Hide previous drawings
        if review_sequence:
            review_sequence.mute = True

        for layer in gp.layers:
            layer.hide = False  # Unhide layer to see it

            # Hide onion skin
            layer.use_annotation_onion_skinning = False

            # Get frames in the sequence range if the frame is not already used as a reference
            sequence_frames = [
                frame
                for frame in layer.frames
                if (
                    frame.frame_number not in {f[0] for f in reference_frames.keys()}
                    and visible_sequence.frame_start
                    <= frame.frame_number
                    < visible_sequence.frame_final_end
                )
            ]

            # Create related annotations
            for i, frame in enumerate(sequence_frames):
                # Move playhead to frame
                scene.frame_set(frame.frame_number)

                # Render only if frame has drawings
                if len(frame.strokes):
                    # Calculate duration. If last GP frame use end frame of sequence
                    if frame is sequence_frames[-1]:
                        annotation_duration = (
                            visible_sequence.frame_final_end - frame.frame_number
                        )
                    else:
                        annotation_duration = (
                            sequence_frames[i + 1].frame_number - frame.frame_number
                        )

                    # Build image name and path
                    annotation_name = f"{media_sequence.name}_{layer.info.replace('.', '-')}_{frame.frame_number}_{str(uuid4())[:4]}"
                    annotation_path = annotations_directory.joinpath(
                        f"{annotation_name}.{scene.render.image_settings.file_format.lower()}"
                    )

                    # Set scene settings for render
                    scene.render.filepath = annotation_path.as_posix()

                    # Export
                    # Override context to force render from the Preview
                    bpy.ops.render.opengl(
                        {"area": bpy.data.screens["Main"].areas[1]},
                        animation=False,
                        sequencer=True,
                        write_still=True,
                    )

                    create_image_annotation(
                        media_sequence,
                        orio_note,
                        frame.frame_number,
                        annotation_duration,
                        annotation_path,
                    )

        # Show previous drawings back
        if review_sequence:
            review_sequence.mute = False

        # Show main sequence and media sequence
        visible_sequence.mute = False
        media_sequence.mute = False
        # ===============

        # Add the note only if has contents, else the note is None
        edited_notes.append(
            (media_sequence, stax_review, orio_note if orio_note.contents else None)
        )

    # Unhide all layers
    for layer in gp.layers:
        layer.hide = False

    #####################
    # Reference images
    #####################

    # Set render settings
    scene.render.image_settings.file_format = "JPEG"

    # Save screenshots of drawn frames and associate them as reference frame to contents
    images_paths = []
    sequences_to_clean = set()
    for reference, contents in reference_frames.items():
        frame_number, sequence = reference
        review_sequence = get_review_sequence(sequence)

        # Build name and path
        media_path = get_source_media_path(sequence)
        image_name = f"{media_path.stem}_retake_{frame_number}"
        image_path = get_temp_directory().joinpath(
            f"{image_name}.{scene.render.image_settings.file_format.lower()}"
        )

        # Set scene
        scene.frame_set(frame_number)
        scene.render.filepath = str(image_path)

        # Unmute sequence's first meta parent
        sequence.mute = False

        # Hide previous drawings
        hidden_sequences = []
        if review_sequence:
            for seq in review_sequence.sequences:
                if not seq.name.startswith("pending"):
                    seq.mute = True
                    hidden_sequences.append(seq)

        # Export
        images_paths.append(image_path)

        preview_area = [  # Override area in case of split view
            x
            for x in bpy.context.screen.areas
            if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
        ][-1]
        bpy.ops.render.opengl(
            {
                "area": preview_area,
            },
            animation=False,
            sequencer=True,
            write_still=True,
        )

        # Add image as reference image to all ImageAnnotation contents
        for content in contents:
            content.reference_image = image_path

        # Keep this sequence to clean it
        sequences_to_clean.add(sequence)

        # Show back hidden previous drawings
        for seq in hidden_sequences:
            seq.mute = False

    # Unmute all
    for sequence in reviewable_sequences:
        sequence.mute = False

    # Set back displayed frame before publishing
    scene.frame_set(current_frame)

    return edited_notes


def build_review_quick_name(media_path: Path) -> str:
    """Build review name to be quickly found.

    :param media_path: Media path to build review name from
    :return: Built name
    """
    hashed_path = md5(bytes(media_path)).hexdigest()

    return f"{media_path.stem[:31]}_{hashed_path}{media_path.suffix}.orio"


def clear_media_reviews(
    sequences_to_clean: List[MediaSequence],
    pending_note_only=False,
    keep_review_link=False,
):
    """Clear reviews or the pending note of given sequences.

    - Delete the review/pending note sequence from the timeline.
    - Remove attributes for reviews handling.

    :param sequences_to_clean: Sequences to clean
    :param pending_note_only: Clear only the pending note data, default to False
    :param keep_review_link: Clear everything but keep the review link
    """
    for media_sequence in sequences_to_clean:
        review_sequence = get_review_sequence(media_sequence)

        # Delete annotations
        sequence_annotate_frames = get_sequence_annotate_frames(media_sequence)
        for layer, frame in sequence_annotate_frames:
            layer.frames.remove(frame)

        # Update existing frames and restore in and out ones
        sequence_annotate_frames = get_sequence_annotate_frames(media_sequence)
        for layer in bpy.data.grease_pencils["Annotations"].layers:
            if pending_note_only:  # Set back empty frames at start and end
                if media_sequence.frame_final_start not in sequence_annotate_frames:
                    layer.frames.new(media_sequence.frame_final_start)
                if media_sequence.frame_final_end not in sequence_annotate_frames:
                    layer.frames.new(media_sequence.frame_final_end)

        # Restore status
        media_sequence.pop("pending_status", None)

        # Set as not reviewed
        media_sequence["reviewed"] = False

        # If no built review sequence, continue
        if not review_sequence:
            continue

        pending_notes_sequences = get_pending_notes_sequences(media_sequence)
        for pending_seq in pending_notes_sequences:  # Delete text comments and replies
            pending_text_contents = pending_seq.get("contents", {})
            for content_type_items in pending_text_contents.values():
                for text in content_type_items:
                    bpy.data.texts.remove(text)

        # Clean timeline
        metas_hierarchy = get_metas_hierarchy(media_sequence)
        substitute_stack = metas_hierarchy[-1]
        parent_stack = metas_hierarchy[-2]

        if pending_note_only:  # Remove pending review elements
            for seq in review_sequence.sequences:
                if seq.name.startswith("pending."):
                    review_sequence.sequences.remove(seq)

            # Refresh TODO bug of Blender check if solved regularily
            bpy.ops.sequencer.refresh_all()
        else:  # Set media sequence raw
            # Remove the whole review sequence
            substitute_stack.sequences.remove(review_sequence)
            media_sequence.pop("review_sequence_name")

            # If it was a stax meta, separate meta TODO replace by .separate() when Blender 3.0
            seq_ed = bpy.context.scene.sequence_editor
            if parent_stack == seq_ed:
                seq_ed.active_strip = substitute_stack
                bpy.ops.sequencer.meta_separate()
            else:
                for seq in substitute_stack.sequences:
                    seq.move_to_meta(parent_stack)

                # Remove substitute stack
                source_channel = substitute_stack.channel
                parent_stack.sequences.remove(substitute_stack)

                # Move back to source channel
                media_sequence.channel = source_channel

            # Clear stax properties
            for property in [
                "orio_review_path" if not keep_review_link else "",
                "current_status",
                "pending_status",
                "reviewed",
            ]:
                media_sequence.pop(property, None)

        # Unlink drawing: scene and gp
        drawing_scene = media_sequence.get("drawing_scene", None)
        if drawing_scene:
            media_sequence.pop("drawing_scene")
            media_sequence.pop("gp")
            bpy.data.scenes.remove(drawing_scene)


def get_link_types_items() -> List[Tuple[str, str, str]]:
    """Return the different link types items.

    :return: List of link types as tuple items (identifier, name, description)
    """
    return [
        ("NEXT_TO", "Next to", "Seek the reviews to link in the media's directory"),
        (
            "DIRECTORY",
            "Directory",
            "Seek the reviews to link in a target directory",
        ),
    ]


def link_media_reviews(
    media_sequences: List[MediaSequence], directory=None
) -> List[MetaSequence]:
    """Link Media Reviews of given sequences depending on the mode.

    - NEXT_TO (default): Look in the same directory as the media a ``media_name.ext.orio`` review path.
    - DIRECTORY (when ``directory`` parameter is provied): Try to match reviews in the directory with the sequences' media filepath.

    :param media_sequences: Sequences to link reviews to
    :param directory: Target directory to link reviews from
    :return: Linked media sequences
    """
    scene = bpy.context.scene

    linked_sequences = []
    already_loaded_reviews = set()
    for media_sequence in media_sequences:
        media_path = get_source_media_path(media_sequence)

        if directory:  # DIRECTORY mode
            # Keep the source directory
            scene.reviews_linked_directory = directory

            # Clear session
            # Clear only if source directory is different from the targeted one
            # Else it's only updating
            if scene.reviews_linked_directory != directory:
                clear_media_reviews(
                    [get_media_sequence(seq) for seq in scene.sequence_editor.sequences]
                )

            # Quickly find review
            reviews_dir = Path(directory)
            review_path = reviews_dir.joinpath(build_review_quick_name(media_path))
            if review_path.is_file():
                media_sequence["orio_review_path"] = review_path.as_posix()
                linked_sequences.append(media_sequence)

            else:  # Review under quick path not found, look into whole directory
                for child in reviews_dir.glob("*.orio"):
                    orio_review = orio.load_media_review(child)
                    if (
                        child not in already_loaded_reviews
                        and orio_review.media == media_path
                    ):
                        media_sequence["orio_review_path"] = child.as_posix()
                        already_loaded_reviews.add(child)
                        linked_sequences.append(media_sequence)
                        break

        else:  # NEXT_TO mode
            sequence_review = media_path.with_suffix(f"{media_path.suffix}.orio")
            if sequence_review.is_dir():
                media_sequence["orio_review_path"] = sequence_review.as_posix()
                linked_sequences.append(media_sequence)

    return linked_sequences


def read_media_reviews(
    media_sequences: List[MediaSequence],
) -> Set[Tuple[MetaSequence, MediaReview]]:
    """Read media reviews of sequences and return them associated.

    :param media_sequences: Media sequences to read reviews of
    :param return: List of tuple for couple (media_sequence, media_review)
    """
    seqs_and_reviews = []
    for media_sequence in media_sequences:
        # Get ORIO review
        review_path = media_sequence.get("orio_review_path")
        if review_path and Path(review_path).exists():
            orio_review = orio.load_media_review(review_path)
        else:
            orio_review = MediaReview(get_source_media_path(media_sequence))

        seq_and_review = (media_sequence, orio_review)
        if seq_and_review not in seqs_and_reviews:
            seqs_and_reviews.append(seq_and_review)

    return seqs_and_reviews


def build_media_reviews(
    sequences_and_reviews: List[MediaSequence],
) -> List[MetaSequence]:
    """Build or update media reviews as VSE sequences.

    Status is kept as dict.

    :param sequences_and_reviews: Media sequences to build reviews of
    """
    created_review_sequences = [None] * len(sequences_and_reviews)

    sequence_editor = bpy.data.scenes["Scene"].sequence_editor
    sequence_editor.active_strip = None
    for i, seq_and_review in enumerate(sequences_and_reviews):
        media_sequence, orio_review = seq_and_review

        if not media_sequence or not orio_review:  # Sentinel
            continue

        # Get review sequence
        review_sequence = media_sequence.get("review_sequence_name")
        if review_sequence:  # Already has a review sequence
            review_sequence = sequence_editor.sequences_all.get(review_sequence)
        else:  # Create a review strip by default
            stack = get_holding_stack(media_sequence)

            # Create packing sequence
            meta = build_substitute(stack, media_sequence)

            # Create review sequence
            review_sequence = meta.sequences.new_meta(
                f"review.{media_sequence.name}",
                media_sequence.channel + 3,  # To leave room for wipe effect sequences
                media_sequence.frame_start,
            )
            review_sequence["orio_source_path"] = media_sequence.get("orio_review_path")
            review_sequence.blend_type = "ALPHA_OVER"
            review_sequence["stax_kind"] = "review"

            # Associate to media sequence
            media_sequence["review_sequence_name"] = review_sequence.name
            review_sequence["media_sequence_name"] = media_sequence.name

        # Set status
        media_sequence["current_status"] = orio_review.status.state
        media_sequence["pending_status"] = media_sequence.get(
            "pending_status"
        )  # Keep set pending status if one

        # Cast to string all values because of blender's lack of type support
        review_sequence["status"] = {
            k: str(v) for k, v in orio_review.status.as_dict().items()
        }

        # TODO iterate all attributes?
        review_sequence["metadata"] = orio_review.metadata

        # Build review content
        last_update = review_sequence.get(
            "update_date"
        )  # TODO check for this behaviour
        last_update = datetime.fromisoformat(last_update) if last_update else None
        new_notes = orio_review.get_notes_list(from_date=last_update)

        build_notes_sequences(media_sequence, review_sequence, new_notes)

        # Keep created review sequence
        created_review_sequences[i] = review_sequence

    return created_review_sequences


def build_notes_sequences(
    media_sequence: MediaSequence, review_sequence: MetaSequence, notes_list: List[Note]
):
    """Build notes as VSE sequences with their contents.

    Only the ImageAnnotation contents are built as image sequences.
    Other contents are kept by type as dicts.

    :param media_sequences: Media sequence to build review's notes of
    :param review_sequence: Review sequence to build notes in
    :param notes_list: List of notes to build
    """
    for channel, note in enumerate(notes_list, review_sequence.channel + 1):
        # Sentinel for existing notes, skip
        if review_sequence.sequences.get(note.date.isoformat()):
            continue

        # Build note sequence
        note_sequence = review_sequence.sequences.new_meta(
            note.date.isoformat(), channel, review_sequence.frame_start
        )
        note_sequence.blend_type = "ALPHA_OVER"
        note_sequence["stax_kind"] = "note"
        note_sequence["author"] = note.author
        if note.parent:  # Set parent note
            note_sequence["parent"] = note.parent.date.isoformat()

        # Build contents
        # ==============

        # Text comments/annotations
        contents_as_dict = {}
        annotation_channel = 1
        for i, content in enumerate(note.contents):

            # Annotations
            if type(content) is ImageAnnotation:
                # Calculate the frame in of the image,
                # strip if started before start of clip
                annotation_start_frame = media_sequence.frame_start + content.frame
                if annotation_start_frame < media_sequence.frame_final_start:
                    frame_end = annotation_start_frame + content.duration
                    if frame_end > media_sequence.frame_final_start:
                        annotation_start_frame = media_sequence.frame_final_start
                    else:  # In the case that the annotation ends before clip start, do not create annotation
                        continue

                # Create image sequence
                image = note_sequence.sequences.new_image(
                    name=f"{note_sequence.name}_annotation.{str(i).zfill(3)}",
                    filepath=f"{content.path_to_image}",
                    channel=annotation_channel,
                    frame_start=annotation_start_frame,
                    fit_method="FIT",
                )
                if image:
                    image.frame_final_duration = content.duration
                    image.blend_type = "ALPHA_OVER"
                    image["stax_kind"] = "annotation"

                    annotation_channel += 1
            else:
                # Create contents type list sentinel
                content_type = content.__class__.__name__
                contents_as_dict.setdefault(content_type, [])
                contents_as_dict[content_type].append(content.as_dict())

            # Associate contents
            note_sequence["contents"] = contents_as_dict

            # Set metadata
            note_sequence["metadata"] = note.metadata

    # Update size of meta sequence
    review_sequence.update()


def extract_media_reviews(
    review_sequences: List[MetaSequence],
) -> List[MediaReview]:
    """Extract media reviews from reviews VSE sequences.

    :param review_sequences: Review sequences to build reviews from
    :return: List of ORIO media reviews
    """
    orio_reviews = []

    for review_seq in review_sequences:
        if not review_seq:  # Sentinel
            continue

        # Get related media sequence
        media_sequence = bpy.context.scene.sequence_editor.sequences_all.get(
            review_seq["media_sequence_name"]
        )

        # Create review
        orio_review = MediaReview(
            get_source_media_path(media_sequence),
            metadata=review_seq["metadata"].to_dict(),
        )

        # Set status
        orio_review.status = orio.from_dict(review_seq["status"].to_dict())

        # Set notes
        append_notes_from_sequences(review_seq, orio_review)

        orio_reviews.append(orio_review)

    return orio_reviews


def append_notes_from_sequences(
    review_sequence: MetaSequence, orio_review: MediaReview
):
    """Extract and append notes from VSE sequences with their contents to the orio review.

    :param review_sequence: Review sequence to build notes from
    :param orio_review: MediaReview object to append notes to
    """
    for note_seq in review_sequence.sequences:

        if note_seq.name.startswith("pending."):  # Sentinel
            continue

        # Create note
        orio_note = Note(
            note_seq["author"],
            date=note_seq.name,
            metadata=note_seq["metadata"].to_dict(),
        )

        # Add parent
        if note_seq.get("parent"):
            orio_note.parent = orio_review.get_note(note_seq["parent"])

        # Set contents
        for contents_list in note_seq[
            "contents"
        ].values():  # TODO refactor structure: drop list by type
            for content in contents_list:
                orio_note.add_content(orio.from_dict(content.to_dict()))

        # ImageAnnotation
        for content_seq in note_seq.sequences:
            orio_note.add_content(
                ImageAnnotation(
                    get_source_media_path(content_seq),
                    content_seq.frame_final_start,
                    content_seq.frame_final_duration,
                )
            )

        # Add note
        orio_review.add_note(orio_note)


def export_notes(
    edited_notes: List[Tuple[MediaSequence, MediaReview, Note]], target_dir: Path = None
):
    """Export notes list as [(Review, Note)].

    Reviews are written next to their related medias using the same path and naming
    with a `.otio` extension (a.k.a NEXT_TO mode).
    If target_dir is provided, all reviews are written in the directory (a.k.a DIRECTORY mode).
    When written, the review path is associated to the media sequence `orio_review_path`.

    TODO Optim: check for duplicate reviews for two different notes.

    :param edited_notes: Notes list to export
    :param target_dir: Directory to write all reviews, defaults to None
    """
    # Write reviews in folder
    if target_dir and not target_dir.is_dir():
        target_dir.mkdir(parents=True)

    for media_sequence, orio_review, orio_note in edited_notes:
        # Add note only if exists
        if orio_note:
            orio_review.add_note(orio_note)

        # Write review to linked review if exists
        review_path = media_sequence.get("orio_review_path")
        if review_path:
            review_path = orio_review.write(review_path)
        else:
            # Write review to target directory and associate to media sequence
            if target_dir:
                review_path = orio_review.write(
                    target_dir.joinpath(build_review_quick_name(orio_review.media))
                )
            else:
                review_path = orio_review.write(f"{orio_review.media}.orio")

        # Associate to sequence
        media_sequence["orio_review_path"] = review_path.as_posix()


def get_sequence_annotate_frames(
    media_sequence: MediaSequence,
) -> List[Tuple[GPencilLayer, GPencilFrame]]:
    """Get frames in the sequence range if the frame is not already used as a reference

    :param media_sequence: Media sequence to get review path from
    :return: List of related Annotate GPencil layer and frame (layer, frame)
    """
    gp = bpy.data.grease_pencils["Annotations"]

    sequence_frames = []
    for layer in gp.layers:
        layer.hide = False  # Unhide layer to see it

        all_frames = list(layer.frames)
        for frame in all_frames:
            if (
                media_sequence.frame_final_start
                <= frame.frame_number
                <= media_sequence.frame_final_end
            ):
                sequence_frames.append((layer, frame))

    return sequence_frames


def get_pending_notes_sequences(media_sequence: MediaSequence) -> List[MetaSequence]:
    """Return the pending note sequence for a given media sequence.

    The pending note sequence holds all the pending note information and is under the review sequence
    associated to the media sequence.

    :param media_sequence: Sequence to get the media sequence from
    :return: List of pending notes sequences
    """
    sequences = []

    # Get pending note in review sequence if exists
    review_sequence = get_review_sequence(media_sequence)

    if review_sequence:
        # Get pending
        for seq in review_sequence.sequences:
            if seq.name.startswith("pending."):
                sequences.append(seq)

    return sequences


def create_note_sequence(media_sequence: MediaSequence) -> MetaSequence:
    """Create a note sequence for a given media sequence under the review sequence.

    :param media_sequence: Sequence to get the media sequence from
    :param create: If the pending note doesn't exist, create it
    :return: List of pending notes sequences
    """
    review_sequence = get_review_sequence(media_sequence)

    # Build empty review sequence for media sequence if doesn't exist
    if not review_sequence:
        created_review_sequences = build_media_reviews(
            [(media_sequence, MediaReview(get_source_media_path(media_sequence)))]
        )
        review_sequence = created_review_sequences[0]

    # Create pending note sequence
    note_sequence = review_sequence.sequences.new_meta(
        f"pending.{media_sequence.name}_{uuid4()}",
        len(review_sequence.sequences) + 1,
        media_sequence.frame_final_start,
    )

    # Set default attributes
    note_sequence["contents"] = {}

    return note_sequence


def get_active_pending_note_sequence(
    media_sequence: MediaSequence, create=False
) -> List[MetaSequence]:
    """Get the active pending note sequence.

    :param media_sequence: Sequence to get the media sequence from
    :param create: If the pending note doesn't exist, create it
    :return: New note
    """
    seq_ed = bpy.context.scene.sequence_editor
    review_sequence = get_review_sequence(media_sequence)

    # Get current pending note sequence
    current_pending_note_seq = None
    if review_sequence and review_sequence.get("active_pending_note_seq"):
        current_pending_note_seq = seq_ed.sequences_all.get(
            review_sequence["active_pending_note_seq"]
        )

    if current_pending_note_seq:
        return current_pending_note_seq
    else:
        if not create:  # Sentinel
            return

        pending_note_sequence = create_note_sequence(media_sequence)

        # Update review sequence
        review_sequence = get_review_sequence(media_sequence)

        # Keep the reference
        review_sequence["active_pending_note_seq"] = pending_note_sequence.name

        return pending_note_sequence


def set_reviewed_state(sequence: MediaSequence):
    """Set "reviewed" state to the sequence.

    False by default but checks all possible review modifications.

    :param sequence: Sequence to be checked
    """
    reviewed = False

    # Pending note sequence created
    pending_note_sequence = get_pending_notes_sequences(sequence)
    if pending_note_sequence:
        reviewed = True

    # Annotate tool
    if not reviewed:
        gp = bpy.data.grease_pencils["Annotations"]

        # Check if annotate frames with strokes are over the sequence
        for layer in gp.layers:
            all_frames = list(layer.frames)
            for frame in all_frames:
                if (
                    sequence.frame_start
                    <= frame.frame_number
                    < sequence.frame_final_end
                    and len(frame.strokes)
                ):
                    reviewed = True

                    break

    # Set reviewed state to sequence
    if reviewed:
        sequence["reviewed"] = reviewed
