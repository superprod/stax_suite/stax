# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to user config management
"""

import json
import os
import platform
from pathlib import Path

import bpy

from .utils_core import (
    report_message,
)


def read_user_config(logger=report_message) -> dict:
    """Read the user config as json file into the app data directory

    :return: User config as dict
    """
    config_path = Path(get_config_directory() + "/cfg.json").resolve()

    # Default config file
    if not config_path.is_file():
        config_data = {}
        logger({"DEBUG"}, "Custom User Configuration not found, default loaded.")
    else:
        with config_path.open("r") as config_stream:
            config_data = json.load(config_stream)

    if hasattr(bpy.context, "view_layer"):
        logger({"DEBUG"}, f"Custom User Configuration found at: {config_path}")

    # Get rid of empty values
    config_data = {k: v for k, v in config_data.items()}

    return config_data


def load_user_config(logger=report_message):
    """Load the user config into Stax configuration user preferences"""
    config_data = read_user_config()

    # Load into user preferences
    if hasattr(bpy.context, "scene"):  # TODO meant to disappear
        for category, properties in config_data.items():
            prefs = getattr(bpy.context.scene, category, None)

            if not prefs:  # Sentinel
                continue

            for k, v in properties.items():
                try:
                    setattr(prefs, k, v)
                except TypeError as err:
                    print(err)
                    print(f"{k}: {v} can't be set for {prefs}")


def write_user_config(prefs_name="", additional_data=dict()):
    """Write the given preferences config as json file into the app data directory.

    :param prefs_name: Extra preferences name
    :param additional_data: Extra custom data to be added in the config
    """
    config_path = Path(get_config_directory(), "cfg.json").resolve()
    config_data = read_user_config()

    if prefs_name:
        prefs = getattr(bpy.context.scene, prefs_name)

        # If property group add it to config data
        if not isinstance(config_data.get(prefs.name), dict):
            config_data[prefs.name] = {}
        for a in prefs.__annotations__:
            # To make sure the enum returns the related string and not the int, use getattr()
            value = getattr(prefs, a, "")

            if (
                type(value).__name__ == "Color"
            ):  # Sentinel for color, cannot use isinstance for a mysterious reason
                value = tuple(value)

            config_data[prefs.name][a] = value

    # Add additionnal data
    config_data.update(additional_data)

    # Create directory if doesn't exit
    if not config_path.parent.is_dir():
        config_path.parent.mkdir(parents=True)

    # Write the config data to the config json file
    with config_path.open("w") as config_stream:
        json.dump(config_data, config_stream, indent=4)


def get_config_directory():
    """Get Stax config directory path

    :return: Stax default cache directory path as string
    """
    return (
        str(Path(os.getenv("APPDATA")).joinpath("Stax"))
        if platform.system() == "Windows"
        else str(Path.home().joinpath("stax"))
    )
