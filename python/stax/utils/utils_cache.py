# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to cache management
"""

import os
import tempfile
from pathlib import Path

import bpy


def get_default_cache_directory():
    """Get Stax default cache directory path

    :return: Stax default cache directory path as string
    """
    return (
        Path(os.getenv("TEMP", tempfile.gettempdir()))
        .joinpath("stax")
        .resolve()
        .as_posix()
    )


def get_cache_directory():
    """Get Stax cache directory path

    :return: Stax cache directory path as string
    """
    user_cache_dir = (
        Path(bpy.context.scene.user_preferences.cache_directory).resolve()
        if hasattr(bpy.context, "scene")
        else get_default_cache_directory()
    )

    return user_cache_dir


def get_orio_default_cache_directory() -> Path:
    """Get Stax default reviews directory path.

    :return: Stax reviews directory
    """
    return Path(get_cache_directory(), "OpenReview").resolve()


def get_temp_directory() -> Path:
    """Get Stax temp directory path.

    :return: Stax temp directory path
    """
    return Path(get_cache_directory(), "tmp")


def save_session():
    """Save the current session by ensuring the default file saving."""
    # Save .blend file
    if bpy.data.filepath:
        bpy.ops.wm.save_mainfile()
    else:  # Default saving
        # Ensure temp dir
        temp_dir = get_temp_directory()
        if not temp_dir.is_dir():
            temp_dir.mkdir(parents=True)

        # Save
        bpy.ops.wm.save_mainfile(
            filepath=temp_dir.joinpath("untitled.blend").as_posix()
        )
