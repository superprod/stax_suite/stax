# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to UI management
"""
import shutil
from pathlib import Path
import re
import textwrap
from typing import Iterable, List, Sequence, Union
from datetime import datetime

import bpy
import bl_ui
from bpy.types import MetaSequence
import openreviewio as orio

import stax
from .utils_core import get_text_body, wrap_text
from .utils_timeline import get_media_sequence, MediaSequence

# Dict for dynamic media info panels
info_panels = {}


def draw_set_preview_range(scene: bpy.types.Scene, layout: bpy.types.UILayout):
    """Draw Set Preview range operator

    By default shows a button to set the preview range with the mouse
    When preview is enabled, it allows to change the value by changing the frames start/end number
    Disabled if versions of a sequence are displayed
    """
    row = layout.row(align=True)
    if not scene.versions_displayed_sequence:
        if scene.use_preview_range:
            row.operator("anim.previewrange_clear", icon="CANCEL", text="")
            row.prop(scene, "frame_preview_start", text="")
            row.prop(scene, "frame_preview_end", text="")
        else:
            # If advanced drawing, only draw the Box Select way
            if bpy.context.workspace is bpy.data.workspaces["Advanced drawing"]:
                layout.operator("anim.previewrange_set", icon="PREVIEW_RANGE")
            else:
                layout.menu("DOPESHEET_MT_preview_range", icon="PREVIEW_RANGE")


def draw_emphasize_drawing_slider(layout):
    """Draw Emphasize drawings slider.

    :param layout: Layout to draw the slider in
    """
    row = layout.row(align=True)
    row.label(icon="LIGHT_SUN")
    row.prop(bpy.context.window_manager, "emphasize_drawings", text="")


def override_annotation_layers():
    """Override annotation layers panel functions to remove some UI

    Needed because AppOverrideState is not fully implemented.

    .. todo:: remove it when the core method is patched:

        template_ID removal and impossibility to disable:

            * ("GPencilLayer", "lock_frame")
            * "gpencil.annotation_active_frame_delete"
    """

    def draw_layers(self, context, layout, gpd):
        row = layout.row()

        col = row.column()
        if len(gpd.layers) >= 2:
            layer_rows = 5
        else:
            layer_rows = 3
        col.template_list(
            "GPENCIL_UL_annotation_layer",
            "",
            gpd,
            "layers",
            gpd.layers,
            "active_index",
            rows=layer_rows,
            sort_reverse=True,
            sort_lock=True,
        )

        col = row.column()

        sub = col.column(align=True)
        sub.operator("gpencil.stax_layer_annotation_add", icon="ADD", text="")
        sub.operator("gpencil.layer_annotation_remove", icon="REMOVE", text="")

        gpl = context.active_annotation_layer
        if gpl:
            if len(gpd.layers) > 1:
                col.separator()

                sub = col.column(align=True)
                sub.operator(
                    "gpencil.layer_annotation_move", icon="TRIA_UP", text=""
                ).type = "UP"
                sub.operator(
                    "gpencil.layer_annotation_move", icon="TRIA_DOWN", text=""
                ).type = "DOWN"

        tool_settings = context.tool_settings
        if gpd and gpl:
            layout.prop(gpl, "thickness")
        else:
            layout.prop(tool_settings, "annotation_thickness", text="Thickness")

    def annotation_layers_panel_draw(self, context):
        layout = self.layout
        layout.use_property_decorate = False

        # Grease Pencil owner
        gpd = context.annotation_data

        # List of layers/notes
        if gpd and gpd.layers:
            draw_layers(self, context, layout, gpd)

    bl_ui.properties_grease_pencil_common.AnnotationDataPanel.draw = (
        annotation_layers_panel_draw
    )


def setup_custom_icons():
    """Copy custom Stax icons to Blender's "datafiles/icons"

    Might encounter an issue if the user has installed Blender in a system directory and (s)he is not admin.
    This is a known issue and this case must be handled by the user.
    """

    blender_icons_dir = Path(bpy.utils.system_resource("DATAFILES"), "icons")
    stax_icons_dir = (
        Path(stax.__file__).parent.parent.parent.joinpath("icons").resolve()
    )

    for icon in stax_icons_dir.iterdir():
        if icon.suffix == ".dat":
            shutil.copy(icon, blender_icons_dir.joinpath(icon.name))


match_url = re.compile(
    (
        "((http|https)://)(www.)?"
        + "[a-zA-Z0-9@:%._\\+~#?&//=]"
        + "{2,256}\\.[a-z]"
        + "{2,6}\\b([-a-zA-Z0-9@:%"
        + "._\\+~#?&//=]*)"
    )
)


def display_line(layout, line) -> bpy.types.UILayout:
    """Display line with smart behaviour.

    - Check for URLs in text and make the line clickable.

    :param layout: Layout in which display the line
    :param line: Line of text to display
    :return: Displayed line layout
    """
    url = match_url.search(line)
    if url:  # Clickable URLs
        layout.operator("wm.open_weburl", text=line, emboss=False).url = url[0]
        layout = layout.row()
    else:
        layout.label(text=line)


def display_note(
    layout: bpy.types.UILayout,
    width: float,
    media_sequence: Sequence,
    stax_note: MetaSequence,
) -> bpy.types.UILayout:
    """Display note content in a box

    :param layout: Layout in which display the note
    :param width: Width of the panel for text wrapping
    :param media_sequence: Media Sequence the note belongs to
    :param stax_note: Stax Note to display contents of
    :return: Created box layout
    """
    contents = stax_note.get("contents", {})
    is_pending_note = stax_note.name.startswith("pending.")

    # Note header
    box = display_note_header(
        layout,
        stax_note.get("author", bpy.context.scene.user_preferences.author_name),
        "" if is_pending_note else stax_note.name,
    )

    # Text contents
    if contents.get("TextComment") or contents.get(
        "TextAnnotation"
    ):  # Sentinel if no text to display
        wrapper = textwrap.TextWrapper(
            width=width, break_long_words=False, replace_whitespace=False
        )

        # Body
        comments = contents.get("TextComment", [])
        for text_comment in comments:
            row = box.row()
            if is_pending_note:  # comment object is a pad
                display_comment(
                    row,
                    wrapper,
                    get_text_body(text_comment),
                )

                # Buttons
                display_edition_buttons(row, text_comment.name, move_playhead=False)

            else:
                display_comment(
                    row,
                    wrapper,
                    text_comment["body"],
                )

        # Annotations
        annotations = contents.get("TextAnnotation", [])
        for text_annotation in annotations:
            row = box.row()
            # Extract from text pad if pending
            if is_pending_note:
                annotation_body = get_text_body(text_annotation)
                # Fake dict annotation for optimization, equivalent of TextAnnotation.as_dict()
                text_annotation_as_dict = {
                    "body": annotation_body,
                    "frame": text_annotation.get("frame_start"),
                    "duration": text_annotation.get("frame_end")
                    - text_annotation.get("frame_start"),
                }

                display_annotation(row, wrapper, text_annotation_as_dict)

                # Buttons
                display_edition_buttons(row, text_annotation.name)
            else:
                display_annotation(row, wrapper, text_annotation)

    # Image Annotations
    image_annotations_seqs = [
        seq for seq in stax_note.sequences if seq.get("stax_kind") == "annotation"
    ]

    if image_annotations_seqs:
        box.label(text="=====")

        # Create column
        col = box.column()
        # Add label
        col.label(
            text=f"Image Annotation{'s' if len(image_annotations_seqs) > 1 else ''}:"
        )
        # Create image annotations
        for i, annot_seq in enumerate(image_annotations_seqs):
            # Return to line when more than 5 annotations
            if i % 5 == 0:
                row = col.row(align=True)

            ops = row.operator(
                "sequencer.show_orio_annotation",
                text=(
                    f"{annot_seq.frame_final_start - media_sequence.frame_final_start}->{annot_seq.frame_final_end - media_sequence.frame_final_start}"
                    if annot_seq.frame_final_duration > 1
                    else f"{annot_seq.frame_final_start - media_sequence.frame_final_start}"
                ),
            )
            ops.frame_start = annot_seq.frame_final_start

    # Grey it out if pending
    box.active = not bool(stax_note.name.startswith("pending."))

    return box


def display_comment(
    layout: bpy.types.UILayout,
    wrapper: textwrap.TextWrapper,
    text_comments: List[str],
) -> bpy.types.UILayout:
    """Display text comments body

    :param layout: Layout in which display the note
    :param wrapper: Wrapper to wrap the text
    :param text_comments: Comments to display
    :return: Created box layout
    """
    col = layout.column(align=True)
    # For comment in text_comments:
    wrapped_lines = wrap_text(wrapper, text_comments)
    for line in wrapped_lines:
        sub = col.row()
        sub.alignment = "EXPAND"
        display_line(sub, line)

    col.separator()

    return col


def display_note_header(layout: bpy.types.UILayout, author: str, date=""):
    """Display note header with user name and date

    :param layout: Layout do display into
    :param author: Author of the note
    :param date: Date of note's creation, defaults to
    """
    # Create box
    box = layout.box()

    # Header
    col = box.column(align=True)
    col.label(text=author, icon="USER")
    if date:
        col.label(
            text=datetime.fromisoformat(date).strftime("%m/%d/%Y, %H:%M:%S"),
            icon="TIME",
        )

    box.separator()

    return box


def display_annotation(
    layout: bpy.types.UILayout,
    wrapper: textwrap.TextWrapper,
    text_annotation: List[Union[orio.Content.TextAnnotation]],
) -> bpy.types.UILayout:
    """Display text annotations

    :param layout: Layout in which display the note
    :param wrapper: Wrapper to wrap the text
    :param text_annotation: Text annotations to display
    :return: Created box layout
    """
    scene = bpy.context.scene
    current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
    media_sequence = get_media_sequence(current_sequence)

    # Display text annotation
    # =======================
    sub_col = layout.column(align=True)

    # Text Annotation Header
    # -------
    row = sub_col.row(align=True)

    # Absolute values, start and end frame are limited to current sequence boundaries if beyond
    raw_annotation_start_frame = (
        media_sequence.frame_start
        - media_sequence.animation_offset_start
        + text_annotation.get("frame")
    )
    absolute_start_frame = max(
        current_sequence.frame_start,
        raw_annotation_start_frame,
    )
    absolute_end_frame = min(
        raw_annotation_start_frame + text_annotation.get("duration"),
        current_sequence.frame_final_end - 1,
    )

    # Button to handle preview range: if the current preview is the same range of the annotation, display a cross to remove it
    if scene.use_preview_range and (
        scene.frame_preview_start == absolute_start_frame
        and scene.frame_preview_end == absolute_end_frame
    ):
        row.prop(scene, "use_preview_range", icon="PREVIEW_RANGE", text="")
    else:
        # Button to automatically set the preview range
        ops = row.operator(
            "sequencer.show_orio_annotation",
            text="",
            icon="PREVIEW_RANGE",
        )

        # if absolute_end_frame - absolute_start_frame > 1:
        ops.frame_start = absolute_start_frame
        ops.frame_end = absolute_end_frame
        ops.set_preview_range = True

    # Label
    # If duration is greater than 0, display the range, else only the frame
    if text_annotation.get("duration") > 1:
        row.label(
            text=f"[{text_annotation.get('frame')} --> {text_annotation.get('frame') + text_annotation.get('duration')}]"
        )
    else:
        row.label(text=f"[{text_annotation.get('frame')}]")
    # -------

    sub_col.separator()

    # Wrap text and display
    wrapped_lines = wrap_text(wrapper, text_annotation.get("body"))

    for line in wrapped_lines:
        row = sub_col.row()
        row.alignment = "LEFT"

        # Display line as operator to be able to click on it
        ops = row.operator(
            "sequencer.show_orio_annotation",
            text=line,
            emboss=False,
            depress=True,
        )
        # Set preview range only if wider than 1
        # if absolute_end_frame - absolute_start_frame > 1:
        ops.frame_start = absolute_start_frame
        ops.frame_end = absolute_end_frame


def display_edition_buttons(
    layout: bpy.types.UILayout, text_obj_name: str, move_playhead=True
):
    """Display edition buttons in a column: Delete, Edit.

    :param layout: Layout to insert the column into
    :param text_obj_name: Text to associate to the buttons
    :param move_playhead: Move playhead to first frame of annotation, defaults to True
    """
    col = layout.column()

    # Delete button
    col.operator(
        "wm.delete_comment", icon="TRASH", text=""
    ).text_obj_name = text_obj_name

    # Edit button, do not move playhead to first frame of comment
    ops = col.operator("wm.edit_comment", icon="LINE_DATA", text="")
    ops.text_name = text_obj_name
    ops.move_playhead = False


def open_text_window(text_obj):
    context = bpy.context

    # List preview areas
    preview_areas = [
        area for area in bpy.data.screens["Console"].areas if area.type == "CONSOLE"
    ]

    # Get the existing window if there is one and delete it
    for win in context.window_manager.windows:
        if win.screen.name.startswith("Writing"):
            writing_window = win
            bpy.ops.wm.window_close({"window": win})
            break

    # Duplicate preview area
    if preview_areas:
        bpy.ops.screen.area_dupli(
            {
                "screen": bpy.data.screens["Console"],
                "area": preview_areas[-1],
                "window": context.window_manager.windows[0],
            },
            "INVOKE_DEFAULT",
        )

    # Get new window
    writing_window = context.window_manager.windows[-1]
    writing_window.screen.name = "Writing"

    # Get wanted area
    area = [x for x in writing_window.screen.areas if x.type == "CONSOLE"][0]

    # Change area type
    area.type = "TEXT_EDITOR"
    area.spaces[0].show_syntax_highlight = False

    # Resize window
    # There's no way to resize window, screen or area in Blender 2.82a API...

    # Load text in the editor
    area.spaces[0].text = text_obj

    # Set cursor to the first line to avoid display issue
    text_obj.cursor_set(0)


def get_stax_icon(icon_name: str) -> bpy.types.ImagePreview:
    """Get Stax custom icon by its name.

    :param icon_name: Name of the wanted icon
    :return: Icon Image Preview
    """
    icons_previews = stax.preview_collections["icons"]
    return icons_previews[icon_name]


def display_info(layout: bpy.types.UILayout, info: dict):
    """Display information tree into layout.

    :param layout: Layout to draw the infos into
    :param info: Information to display
    """
    for key, value in info.items():
        # Iterate nested values or display them
        if type(value) not in [str, list] and isinstance(value, Iterable):  # Iterate
            box = layout.box()
            box.label(text=key, icon="RIGHTARROW")
            display_info(box, value)

        elif type(value) is list:  # Display list
            # Box for list
            box = layout.box()
            box.label(text=key)

            # Column split
            col = box.column(align=True)
            split = col.split(factor=0.05)
            split.column()
            right = split.column()

            # Display elements
            for element in value:
                # Iterate nested iterables
                if type(value) not in [str, list] and isinstance(value, Iterable):
                    box = layout.box()
                    box.label(text=key, icon="DOWNARROW")
                    display_info(box, value)

                # Display element
                else:
                    row = right.row()
                    display_line(row, element)

        else:  # Display unique value
            row = layout.row()

            # Red Color
            if type(value) in [bool, int] and (not value or value < 0):
                row.alert = True

            display_line(row, f"{key}: {value}")


def dyn_panel_draw(self, _):
    """Draw function for info display dynamic panels."""
    layout = self.layout

    display_info(layout, self.informations)


class PREVIEW_PT_category_info(bpy.types.Panel):
    """All current media info sub panels"""

    bl_label = "Media Info"
    bl_region_type = "UI"
    bl_category = "Info"
    bl_space_type = "SEQUENCE_EDITOR"
    bl_parent_id = "PREVIEW_PT_media_info"


def update_media_info_panels(media_sequence: MediaSequence):
    """Create panels for media info depending on the "stax_media_info" property contents of the sequence.

    :param sequence: Sequence handling the media info
    """
    info_to_display = media_sequence.get("stax_media_info", {})

    if info_to_display:  # Create panels for info data
        try:
            for key, value in info_to_display.items():
                # Dynamically create panel class
                panel = type(
                    f"PREVIEW_PT_{key}",
                    (PREVIEW_PT_category_info,),
                    {
                        "bl_label": key,
                        "bl_description": f"{key} informations",
                        "informations": value,
                        "draw": dyn_panel_draw,
                    },
                )

                # Unregister panel and remove it
                if "bl_rna" in panel.__dict__:
                    bpy.utils.unregister_class(panel)
                    info_panels.pop(panel.__name__)

                # Register panel and keep reference
                bpy.utils.register_class(panel)
                info_panels[panel.__name__] = panel

        except Exception as e:
            bpy.ops.wm.report_message(type="WARNING", message=e)

    else:  # Delete previously created panels
        for panel in info_panels.values():
            bpy.utils.unregister_class(panel)

        info_panels.clear()


def exit_advanced_drawing_ui(context, source_scene):
    """Return to sequence editor Main view"""
    # Set default scene back
    context.window.scene = source_scene

    # Set Main workspace
    context.window.workspace = bpy.data.workspaces["Main"]
