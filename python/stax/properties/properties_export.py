# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every class property relative to the export tool
"""

import bpy


class Shot(bpy.types.PropertyGroup):
    """Shot"""

    name: bpy.props.StringProperty(
        name="Shot name",
    )
    display: bpy.props.BoolProperty(
        name="Mute the shot",
        description="To be exported",
    )


def name_update(self, context):
    scene = context.scene

    # Tracks are fulfilled
    use_tracks = hasattr(scene, "tracks")

    # Select render track or remove it
    if use_tracks:
        self.channel = scene.tracks.find(self.name) + 1

    # Create shots
    for seq in scene.sequence_editor.sequences:
        if seq.channel == self.channel:
            shot = self.shots.add()
            shot.name = seq.name
            shot.display = not seq.mute


class RenderTrack(bpy.types.PropertyGroup):
    """Render track settings and functions"""

    name: bpy.props.StringProperty(
        name="Track name", subtype="FILE_NAME", update=name_update
    )
    comment: bpy.props.StringProperty(name="Custom comment", subtype="FILE_NAME")
    channel: bpy.props.IntProperty(
        name="Channel to export",
        description="To be used when track has no name",
        default=1,
        min=1,
        max=32,
    )
    shots: bpy.props.CollectionProperty(type=Shot)

    def remove(self):
        scene = bpy.context.scene

        # Remove track
        index = scene.render_tracks.find(self.name)
        scene.render_tracks.remove(index)


classes = [Shot, RenderTrack]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.render_tracks = bpy.props.CollectionProperty(type=RenderTrack)


def unregister():
    del bpy.types.Scene.render_tracks

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
