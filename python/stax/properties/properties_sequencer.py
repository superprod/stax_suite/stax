# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every class relative to sequencer behavior
"""

import bpy
from bpy.types import WipeSequence
from operator import attrgetter

from stax.utils.utils_reviews import available_statuses_callback
from stax.utils.utils_timeline import (
    add_single_driver,
    add_wipe_effect,
    emphasize_drawings,
    compare_refresh,
    get_holding_stack,
    get_media_sequence,
    get_metas_hierarchy,
    get_wipe_sequence,
)

# Sequences to update for wipe and blend type
seqs_to_update = []
# Sequences muted for wipe
seqs_muted_for_wipe = []


def compare_setup():
    """Change blend type of all strips on the same channel.

    Run after any change of compare type in a progressive way, 10 by 10 sequences.

    TODO strange behaviour with nested sequences' alpha driven. Report to Blender
    TODO refactor when foreach_set works for enums
    """
    context = bpy.context
    screen = context.screen
    if screen.is_animation_playing:  # Playback sentinel
        return 1

    # Reset wipe values to avoid persistent wipe
    scene = context.scene
    compare_properties = scene.compare_properties
    if compare_properties.compare_type == "WIPE":
        compare_properties.wipe_fader = 0.5
    else:
        compare_properties.wipe_fader = 0

    def reset_wipe(wipe_sequence: WipeSequence):
        """Reset wipe value and remove driver"""
        wipe_sequence.driver_remove("effect_fader")
        wipe_sequence.effect_fader = 1

    global seqs_to_update
    global seqs_muted_for_wipe
    seq_ed = scene.sequence_editor
    current_sequence = seq_ed.active_strip
    sequences_count = len(seqs_to_update)
    amount_of_seqs_to_update = 10
    for i in range(min(amount_of_seqs_to_update, sequences_count)):
        sequence = seqs_to_update[i]
        media_sequence = get_media_sequence(sequence)

        if not media_sequence:  # Sentinel
            continue

        wipe_sequence = get_wipe_sequence(media_sequence)
        if (
            sequence.channel == current_sequence.channel
        ):  # Apply same blend to strips on same channel

            # Reset wipe
            if wipe_sequence and compare_properties.compare_type != "WIPE":
                reset_wipe(wipe_sequence)

            # Wipe
            if compare_properties.compare_type == "WIPE":

                if not wipe_sequence:  # Create wipe
                    wipe_sequence = add_wipe_effect(media_sequence)

                # Mute sequences under displayed media seq if needed
                meta_hierarchy = get_metas_hierarchy(media_sequence)
                parent_meta = meta_hierarchy[-1]
                current_displayed_stack = (
                    seq_ed.meta_stack[-1] if seq_ed.meta_stack else seq_ed
                )
                for meta in meta_hierarchy:
                    if meta is not parent_meta:
                        for seq in meta.sequences:
                            if (
                                seq is not media_sequence
                                and seq not in meta_hierarchy
                                and not current_displayed_stack.sequences.get(seq.name)
                            ):
                                seqs_muted_for_wipe.append(seq)
                                seq.mute = True

                # Add driver
                add_single_driver(
                    wipe_sequence,
                    "effect_fader",
                    "SCENE",
                    scene,
                    "compare_properties.wipe_fader",
                )

                # Change sequence blend type
                sequence.blend_type = "ALPHA_OVER"
            else:
                # Restore muted sequences for wipe
                for seq in seqs_muted_for_wipe:
                    seq.mute = False
                seqs_muted_for_wipe.clear()

                # Change sequence blend type
                if sequence.blend_type != compare_properties.compare_type:
                    sequence.blend_type = compare_properties.compare_type

        else:
            # Restore muted sequences for wipe
            for seq in seqs_muted_for_wipe:
                seq.mute = False
            seqs_muted_for_wipe.clear()

            if sequence.blend_type != "CROSS":
                sequence.blend_type = "CROSS"

            # Reset wipe
            if wipe_sequence:
                reset_wipe(wipe_sequence)

    # Update list of seqs to keep update
    seqs_to_update = seqs_to_update[amount_of_seqs_to_update:]

    # Set current sequence active back TODO must be dropped when all API entries exist and no more operators are used in functions
    if current_sequence:
        bpy.ops.sequencer.select_all(action="DESELECT")
        seq_ed.active_strip = current_sequence
        current_sequence.select = True

    # Stop if finished
    if len(seqs_to_update) == 0:
        return

    return 0.1


def compare_update(compare_properties, context):
    """Keep all main sequences to update comparison type.

    Run after any change of compare type.

    TODO strange behaviour with nested sequences' alpha driven. Report to Blender

    :param compare_properties: Blender sequencer properties object
    :param context: Blender context
    """
    scene = context.scene
    sequences_stack = (
        scene.sequence_editor.meta_stack[-1].sequences
        if scene.sequence_editor.meta_stack
        else scene.sequence_editor.sequences
    )

    global seqs_to_update

    # Sort sequences from bottom left to top right
    sequences = sorted(sequences_stack, key=attrgetter("channel", "frame_final_start"))
    sequences_count = len(sequences)

    # Find current sequence index into sorted list
    current_seq_index = sequences.index(scene.sequence_editor.active_strip)

    # Split sorted sequences into chunks
    seq_per_chunk = 10
    chunks = [
        sequences[i : i + seq_per_chunk]
        for i in range(0, sequences_count, seq_per_chunk)
    ]
    chunks_count = len(chunks)

    # Order chunks from current sequence chunk to spread on both sides and below into a list
    index = int(current_seq_index / 10)
    seqs_to_update = chunks[index]
    for i in range(1, chunks_count):
        bwd_index, fwd_index = index - i, index + i
        seqs_to_update += (chunks[bwd_index] if bwd_index >= 0 else []) + (
            chunks[fwd_index] if fwd_index < chunks_count else []
        )

    # Run timer for flag
    bpy.app.timers.register(compare_setup, first_interval=0.1)


def change_media_status(self, context):
    """Change status of the current displayed media"""
    media_sequence = get_media_sequence(
        bpy.data.scenes["Scene"].sequence_editor.active_strip
    )
    if media_sequence:
        media_sequence["pending_status"] = self.media_status
        media_sequence["reviewed"] = self.media_status != media_sequence.get(
            "current_status", "waiting review"
        )

    # Start review session
    context.scene.review_session_active = True


class CompareProperties(bpy.types.PropertyGroup):
    """All compare properties"""

    compare_type: bpy.props.EnumProperty(
        items=[
            ("CROSS", "Default", "Displays the sequence and opacity can be modified"),
            (
                "DIFFERENCE",
                "Difference",
                "Subtracts the bottom layer from the top layer",
            ),
            ("MULTIPLY", "Multiply", "Multiply with below sequence"),
            ("WIPE", "Wipe", "Adjustable Wipe with below sequence"),
        ],
        name="Choose blend type",
        default="CROSS",
        update=compare_update,
    )

    opacity: bpy.props.FloatProperty(
        name="Opacity",
        description="Change transparency",
        default=1,
        min=0,
        max=1,
        subtype="FACTOR",
        update=compare_refresh,
    )

    wipe_fader: bpy.props.FloatProperty(
        name="Wipe Fader",
        description="Position of Wipe split",
        default=0.5,
        min=0,
        max=1,
        subtype="FACTOR",
        update=compare_refresh,
    )

    wipe_angle: bpy.props.FloatProperty(
        name="Wipe Angle",
        description="Angle of Wipe split",
        default=-1.570796,  # Radians value for vertical split consistent with left->right slider
        min=-1.570796,
        max=1.570796,
        subtype="ANGLE",
        update=compare_refresh,
    )

    wipe_blur: bpy.props.FloatProperty(
        name="Wipe Blur",
        description="Blur smoothing of Wipe split",
        default=0,
        min=0,
        max=1,
        subtype="FACTOR",
        update=compare_refresh,
    )


def update_media_current_frame(self, context):
    """Update scene current frame when media current frame is set."""
    scene = context.scene
    media_sequence = get_media_sequence(scene.sequence_editor.active_strip)
    scene.frame_set(
        min(
            media_sequence.frame_start + self.media_current_frame,
            media_sequence.frame_final_end,
        )
    )


classes = [CompareProperties]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.all_annotations_displayed = bpy.props.BoolProperty(default=True)
    bpy.types.Scene.compare_properties = bpy.props.PointerProperty(
        type=CompareProperties
    )
    bpy.types.Scene.versions_displayed_sequence = bpy.props.StringProperty(
        description="Keep the name of the sequence the versions are displayed."
    )

    bpy.types.Scene.media_status = bpy.props.EnumProperty(
        items=available_statuses_callback,
        name="Media status",
        update=change_media_status,
    )

    bpy.types.Scene.media_current_frame = bpy.props.IntProperty(
        name="Current media frame",
        update=update_media_current_frame,
        min=0,
    )

    bpy.types.WindowManager.emphasize_drawings = bpy.props.FloatProperty(
        name="Emphasize Drawings",
        description="Increase the whiteness of the media to see the drawings better",
        default=0,
        min=0,
        max=100,
        subtype="PERCENTAGE",
        update=emphasize_drawings,
    )

    bpy.types.WindowManager.newer_to_older = bpy.props.BoolProperty(
        name="Newer to Older", description="Order notes from newer to older"
    )

    bpy.types.Sequence.pending = bpy.props.BoolProperty(
        name="Pending",
        description="Sequence has been created during the current review session",
    )


def unregister():
    del bpy.types.WindowManager.emphasize_drawings
    del bpy.types.Scene.media_status
    del bpy.types.WindowManager.newer_to_older
    del bpy.types.Scene.versions_displayed_sequence
    del bpy.types.Scene.compare_properties
    del bpy.types.Scene.all_annotations_displayed
    del bpy.types.Sequence.pending

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
