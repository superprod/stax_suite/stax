# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every class related to core architecture design
"""
import getpass

import bpy
from bpy.app.handlers import persistent
from bpy.types import WipeSequence

from stax.utils.utils_config import write_user_config
from stax.utils import utils_cache
from stax.utils.utils_reviews import set_reviewed_state
from stax.utils.utils_timeline import (
    add_single_driver,
    add_wipe_effect,
    get_available_tracks,
    get_current_sequence,
    get_media_sequence,
    get_sequences_under_playhead,
    get_wipe_sequence,
    update_preview_channel,
)
from stax.utils.utils_ui import update_media_info_panels


@persistent
def update_viewer_displayed_track(self, context):
    """Update displayed track

    Pass by function.

    :param context: Current context"""
    update_preview_channel()


def current_track_items_callback(self, context):
    """Get items for enum property from available tracks

    :param screen: Blender current screen object
    :param context: Blender context
    """
    available_tracks = get_available_tracks()
    # TODO {str(channel), name} system not very clean, must be inversed

    # Build items list
    return [
        (str(channel), name, "No description")
        for channel, name in available_tracks.items()
    ]


# ======== Handlers ===========


@persistent
def frame_change_update(scene):
    """Run everytime a frame is changed but the animation is not playing.

    Keep dynamic information up-to-date
    """
    if not bpy.data.screens["Main"].is_animation_playing:
        # Deselect the previous sequence if it's not updated to avoid selecting them all while playing
        previous_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        if previous_sequence:
            previous_sequence.select = previous_sequence.get("updated", False)

        # Update the current sequence
        current_sequence = get_current_sequence()

        # Set the current sequence as active
        if scene.sequence_editor:
            scene.sequence_editor.active_strip = current_sequence

        if current_sequence:
            # Select the current sequence
            current_sequence.select = True

            media_sequence = get_media_sequence(current_sequence)

            # Sentinel
            if not media_sequence:
                return

            # Change status selector value to sequence's
            # -------------------

            # Keep the review session state because changing the media status is supposed to start it
            keep_review_session_state = scene.review_session_active

            # Set the "change media status" tool to the current sequence status
            sequence_status = media_sequence.get(
                "pending_status"
            ) or media_sequence.get("current_status", "waiting review")
            if sequence_status and media_sequence.get(
                "pending_status"
            ) != media_sequence.get("current_status"):
                scene.media_status = sequence_status

            # Set back the review session state
            scene.review_session_active = keep_review_session_state
            # --------------------

            # Show/Hide annotations depending on the lock
            bpy.data.screens["Main"].areas[1].spaces[0].show_annotation = (
                scene.all_annotations_displayed and not media_sequence.lock
            )

            # Check if is reviewed
            set_reviewed_state(media_sequence)

            # Update media info panels
            update_media_info_panels(media_sequence)

            # Update media current frame
            media_current_frame = scene.frame_current - media_sequence.frame_start
            if (
                scene.frame_current
                != scene.media_current_frame + media_sequence.frame_start
            ):
                scene.media_current_frame = media_current_frame

            # Change blend to CROSS as default TODO REPLACE might become deprecated and CROSS become default
            if current_sequence.blend_type == "REPLACE":
                current_sequence.blend_type = "CROSS"


class UserPreferences(bpy.types.PropertyGroup):
    """Every preferences Stax needs to save"""

    name: bpy.props.StringProperty(default="user_preferences")

    author_name: bpy.props.StringProperty(
        name="Author Name",
        default=getpass.getuser(),
        description="Name to be used as author for the edited notes",
        update=lambda _, __: write_user_config(prefs_name="user_preferences"),
    )

    cache_directory: bpy.props.StringProperty(
        name="Cache directory",
        default=utils_cache.get_default_cache_directory(),
        subtype="DIR_PATH",
        description="Path to Stax cache directory",
        update=lambda _, __: write_user_config(prefs_name="user_preferences"),
    )

    cache_media: bpy.props.BoolProperty(
        name="Copy media in cache",
        description="Copy all source media from timeline in Stax' cache",
        update=lambda _, __: write_user_config(prefs_name="user_preferences"),
    )

    cache_expiration_delay: bpy.props.IntProperty(
        name="Cache expiration delay",
        description="Delay in days before an untouched timeline cache is automatically removed.",
        default=30,
        update=lambda _, __: write_user_config(prefs_name="user_preferences"),
    )

    advanced_ui: bpy.props.BoolProperty(
        name="Advanced UI",
        description="Show advanced Stax usage UI buttons",
        update=lambda _, __: write_user_config(prefs_name="user_preferences"),
    )

    show_blender_ui: bpy.props.BoolProperty(
        name="Show Blender UI",
        description="Show the original Blender UI",
        update=lambda _, __: write_user_config(prefs_name="user_preferences"),
    )

    autosave_delay: bpy.props.FloatProperty(
        name="Autosave Delay",
        description="Elapsing time in minutes between two autosaves",
        default=5.0,
        min=1,
        unit="TIME",
        update=lambda _, __: write_user_config(prefs_name="user_preferences"),
    )

    # === Colors ===
    image_annotation_marker_color: bpy.props.FloatVectorProperty(
        name="Image annotation marker color",
        description="Color of image annotation marker in the sequencer",
        default=(0.03, 0.15, 0.91),
        subtype="COLOR",
        update=lambda _, __: write_user_config(prefs_name="user_preferences"),
    )

    pending_image_annotation_marker_color: bpy.props.FloatVectorProperty(
        name="Pending image annotation marker color",
        description="Color of pending image annotation marker in the sequencer",
        default=(0.04, 0.01, 0.53),
        subtype="COLOR",
        update=lambda _, __: write_user_config(prefs_name="user_preferences"),
    )


class StaxInfo(bpy.types.PropertyGroup):
    """Informations related to Stax"""

    version: bpy.props.StringProperty(
        name="Stax version",
        description="Stax version that generated this '.blend' file.",
    )

    session_last_update: bpy.props.StringProperty(
        name="Last update",
        description="Date of the '.blend' session file last update.",
    )


classes = [
    UserPreferences,
    StaxInfo,
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.stax_info = bpy.props.PointerProperty(type=StaxInfo)
    bpy.types.Scene.current_track = bpy.props.EnumProperty(
        items=current_track_items_callback,
        name="Choose current track",
        update=update_viewer_displayed_track,
    )
    bpy.types.Scene.user_preferences = bpy.props.PointerProperty(type=UserPreferences)
    bpy.types.Scene.review_session_active = bpy.props.BoolProperty()

    # Set handler
    bpy.app.handlers.frame_change_post.append(frame_change_update)


def unregister():
    del bpy.types.Scene.review_session_active
    del bpy.types.Scene.user_preferences
    del bpy.types.Scene.stax_info
    del bpy.types.Scene.current_track

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
