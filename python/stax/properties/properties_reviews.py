# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every class relative to sequencer behavior
"""

import bpy
from bpy.props import EnumProperty
from bpy.types import PropertyGroup

from stax.utils.utils_config import write_user_config
from stax.utils.utils_reviews import get_link_types_items


class ReviewsProperties(PropertyGroup):
    """All reviews properties"""

    name: bpy.props.StringProperty(default="reviews_properties")

    link_type: EnumProperty(
        items=get_link_types_items(),
        name="Reviews Link Type",
        default="NEXT_TO",
        update=lambda _, __: write_user_config(prefs_name="reviews_properties"),
    )


classes = [ReviewsProperties]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.reviews_properties = bpy.props.PointerProperty(
        type=ReviewsProperties
    )
    bpy.types.Scene.reviews_linked_directory = bpy.props.StringProperty(
        name="Reviews Linked Directory",
        description="Directory to link the reviews from. Link and publish reviews into this directory",
    )


def unregister():
    del bpy.types.Scene.reviews_properties
    del bpy.types.Scene.reviews_linked_directory

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
