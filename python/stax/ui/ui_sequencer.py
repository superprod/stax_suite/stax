# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

import bpy
import bl_ui
import bpy.utils.previews
from bpy.types import Header
from bpy.utils import register_class, unregister_class

from . import ui_drawing
from .ui_notes import NOTES_PT_pending_text_note, NOTES_PT_text_notes
from stax.utils.utils_timeline import (
    get_available_tracks,
    get_current_sequence,
    get_media_sequence,
    get_review_sequence,
)
from stax.utils.utils_ui import (
    draw_emphasize_drawing_slider,
    draw_set_preview_range,
    get_stax_icon,
)


class SEQUENCER_HT_header(Header):
    """Sequencer space header"""

    bl_space_type = "SEQUENCE_EDITOR"

    def draw(self, context):
        space_data = context.space_data
        user_preferences = context.scene.user_preferences

        # Sentinel if scene is not main scene (i.e Advanced Drawing mode)
        if context.scene is not bpy.data.scenes["Scene"]:
            return

        if user_preferences.show_blender_ui:
            # Draw original Blender UI
            bl_ui.space_sequencer.SEQUENCER_HT_header.draw(self, context)

        else:
            # Displayed in Sequencer
            if space_data.view_type == "SEQUENCER":
                self.draw_sequencer(context)

            elif space_data.view_type == "PREVIEW":  # Displayed in Preview
                self.draw_preview(context)

    def draw_sequencer(self, context):
        """Draw UI for Sequencer space"""
        layout = self.layout
        current_sequence = get_current_sequence()

        layout.separator_spacer()

        # Preview range
        draw_set_preview_range(context.scene, layout)

        layout.separator_spacer()

        row = layout.row(align=True)

        # Toggle meta
        if current_sequence:
            row.operator(
                "sequencer.toggle_meta",
                icon="SEQ_STRIP_META",
            )

        # Frame All Clips Button
        layout.separator()
        layout.operator("sequencer.view_all", text="Frame all")

    def draw_preview(self, context):
        """Draw UI for Preview space"""
        layout = self.layout

        scene = context.scene
        current_sequence = get_current_sequence(context.space_data.display_channel)

        layout.separator_spacer()

        # Sentinel if scene is not main scene (i.e Advanced Drawing mode)
        # Draw same tools as in Drawing workspace
        if scene is not bpy.data.scenes["Scene"]:
            ui_drawing.VIEW3D_HT_header.draw_stax_tools(self)
            layout.separator_spacer()
            return
        else:
            # Display media name in preview
            if current_sequence:
                media_sequence = get_media_sequence(current_sequence)
                if media_sequence:
                    is_reviewed = "*" if media_sequence.get("reviewed") else ""
                    layout.label(text=f"{is_reviewed} {media_sequence.name}")

        layout.separator_spacer()

        # Current strip Fusion types and alpha
        layout.popover(panel="SEQUENCER_PT_compare", text="", icon="MOD_BEVEL")

        layout.separator()

        # Emphasize drawings value
        draw_emphasize_drawing_slider(layout)

        layout.separator()

        # Stax Sequencer view tools
        row = layout.row(align=True)
        row.operator("wm.splitview", text="", icon="UV_ISLANDSEL")
        row.operator("wm.detachview", text="", icon="WINDOW")


class SEQUENCER_HT_tool_header(Header):
    """Sequencer tool header"""

    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "TOOL_HEADER"

    def draw(self, context):
        space_data = context.space_data

        # Displayed in Sequencer
        if space_data.view_type == "SEQUENCER":
            self.draw_sequencer(context)

        elif space_data.view_type == "PREVIEW":  # Displayed in Preview
            self.draw_preview(context)

    def draw_sequencer(self, context):
        """Draw UI for Sequencer space"""
        layout = self.layout
        scene = context.scene

        layout.separator_spacer()

        # Sentinel if scene is not main scene (i.e Advanced Drawing mode)
        # Don't display Main UI and inform the user
        if scene is not bpy.data.scenes["Scene"]:
            layout.label(
                text="You are currently in Advanced Drawing mode, please complete or cancel your drawing",
                icon="INFO",
            )
            layout.separator_spacer()
            return

        # Display status changer
        active_strip = scene.sequence_editor.active_strip

        # Change status
        row = layout.row(align=True)
        row.prop(scene, "media_status")

        # Is review enabled
        row.enabled = bool(
            active_strip and not active_strip.lock and get_media_sequence(active_strip)
        )

        layout.separator_spacer()

    def draw_preview(self, context):
        """Draw UI for Preview space"""
        scene = context.scene
        sequence_editor = scene.sequence_editor
        space_data = context.space_data
        layout = self.layout

        # Sentinel if scene is not main scene (i.e Advanced Drawing mode)
        # Don't dislay Main UI
        if scene is not bpy.data.scenes["Scene"]:
            layout.separator_spacer()
            ui_drawing.VIEW3D_HT_header.draw_stax_tools(self)
            layout.separator_spacer()
            return

        row = layout.row()
        row.operator("sequencer.refresh_all", text="", icon="FILE_REFRESH")

        # Select displayed track
        available_tracks = get_available_tracks()
        row.prop(
            scene,
            "current_track",
            text="",
            icon="SEQ_SEQUENCER",
            icon_only=True,
        )

        # Display guessed track name
        row.label(text=available_tracks.get(space_data.display_channel, "All"))

        # Detect if right panel of side by side
        is_right_panel = False
        sq_preview_areas = [
            x
            for x in context.screen.areas
            if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
        ]
        if len(sq_preview_areas) > 1 and context.area == sq_preview_areas[0]:
            is_right_panel = True

        layout.separator_spacer()

        if not is_right_panel:  # Display only in left panel
            active_strip = bpy.data.scenes["Scene"].sequence_editor.active_strip
            media_sequence = get_media_sequence(active_strip)
            # If review is enabled
            if media_sequence and not media_sequence.lock:
                # Active Tool
                # -----------
                from bl_ui.space_toolsystem_common import ToolSelectPanelHelper

                tool = ToolSelectPanelHelper.draw_active_tool_header(context, layout)

                # If tool is annotate
                if tool.idname.startswith("builtin.annotate"):
                    # Display annotation layers
                    gpd = context.annotation_data
                    if gpd:
                        text = ""
                        if gpd.layers.active_note:
                            text = gpd.layers.active_note
                            maxw = 25
                            if len(text) > maxw:
                                text = text[: maxw - 5] + ".." + text[-3:]

                        layout.popover(
                            panel="TOPBAR_PT_annotation_layers",
                            text=text,
                        )

                    # Hack to keep the current tool name
                    context.screen["current_tool"] = tool.idname

                    # Add empty keyframe for current layer
                    layout.operator("scene.add_empty_gp_frame", icon="KEYFRAME_HLT")

                    layout.separator()

                    # Toggle annotations drawing keyframes editing
                    layout.operator(
                        "sequencer.toggle_annotations_keyframes",
                        text="Annotations Keyframes Editing",
                        icon="GP_MULTIFRAME_EDITING",
                    )

            else:
                # If review disabled notify the user
                row = layout.row()
                row.alert = True
                if sequence_editor.active_strip:  # Review disabled
                    if media_sequence:  # Unsupported media
                        row.label(
                            text="You're not allowed to review this media", icon="ERROR"
                        )
                    else:
                        row.label(
                            text="Unsupported media type for review", icon="ERROR"
                        )
                elif sequence_editor.sequences:  # No active media
                    row.label(
                        text="Display a media to review it",
                        icon="ERROR",
                    )
                row.alert = False

            layout.separator_spacer()

            # Show/Hide section
            row = layout.row()
            row.label(text="Show/Hide")
            # Toggle annotations
            review_sequence = (
                get_review_sequence(media_sequence) if media_sequence else None
            )
            if review_sequence:
                row.operator(
                    "sequencer.display_reviews_drawings",
                    text="",
                    icon="STROKE",
                    depress=not review_sequence.mute,
                ).action = (
                    "SHOW" if review_sequence.mute else "HIDE"
                )

        # Toggle text notes
        row.prop(space_data, "show_region_ui", icon="INFO", text="")


class SEQUENCER_PT_compare(bpy.types.Panel):
    """Tools to compare media"""

    bl_label = "Media Comparison Tools"
    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "HEADER"
    bl_ui_units_x = 5

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        compare_properties = scene.compare_properties
        active_strip = scene.sequence_editor.active_strip

        # Current strip Fusion types and alpha
        if active_strip:
            layout.label(text="Compare Tools")

            layout.prop(scene.compare_properties, "compare_type", text="")

            # Wipe
            if compare_properties.compare_type == "WIPE":
                layout.prop(compare_properties, "wipe_fader", text="Split")
                layout.prop(compare_properties, "wipe_angle", text="Angle")
                layout.prop(compare_properties, "wipe_blur", text="Blur")

            # Alpha setting
            if compare_properties.compare_type == "CROSS":
                layout.prop(compare_properties, "opacity", text="Alpha")

        else:
            layout.label(text="No active strip")


class SEQUENCER_MT_context_menu(bpy.types.Menu):
    """Override of sequencer's default right click context menu"""

    bl_label = "Nothing right click. Ask developers to implement something."

    def draw(self, context):
        return


class DOPESHEET_MT_preview_range(bpy.types.Menu):
    bl_idname = "DOPESHEET_MT_preview_range"
    bl_label = "Set Preview Range"

    def draw(self, context):
        layout = self.layout
        layout.operator("anim.previewrange_set", icon="SELECT_SET", text="Box Select")
        layout.operator(
            "sequencer.set_preview_range_selected_sequences",
            icon="NLA",
            text="Selected",
        )


class PREVIEW_PT_stax_tools(bpy.types.Panel):
    """Add a custom tool panel in the Preview toolbar"""

    bl_label = " "
    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "TOOLS"
    bl_parent_id = "SEQUENCER_PT_tools_active"
    bl_options = {"HIDE_HEADER"}

    def draw(self, context):
        layout = self.layout
        layout.scale_y = 1.5

        # Advanced drawing
        layout.operator("wm.start_advanced_drawing", icon="STROKE", text="")

        # Write text comment
        write_comment_icon = get_stax_icon("write_comment")
        layout.operator(
            "wm.write_comment", icon_value=write_comment_icon.icon_id, text=""
        )

        # Edit in external image editor
        if context.preferences.filepaths.image_editor:
            layout.operator(
                "wm.edit_in_image_editor",
                icon="SCREEN_BACK",
                text="",
            )
        else:
            layout.operator(
                "wm.select_external_image_editor",
                icon="WORKSPACE",
                text="",
            )

        layout.separator()

        # Clear drawings
        layout.operator("scene.clear_pending_note", icon="TRASH", text="")


class PREVIEW_PT_pending_text_note(NOTES_PT_pending_text_note):
    """Pending Text note into Preview"""

    bl_space_type = "SEQUENCE_EDITOR"


class PREVIEW_PT_text_notes(NOTES_PT_text_notes):
    """Text notes into Preview"""

    bl_space_type = "SEQUENCE_EDITOR"


class PREVIEW_PT_media_info(bpy.types.Panel):
    """All current media info panel"""

    bl_label = "Media Info"
    bl_region_type = "UI"
    bl_category = "Info"
    bl_space_type = "SEQUENCE_EDITOR"

    def draw(self, context):
        pass


classes = (
    SEQUENCER_HT_header,
    SEQUENCER_HT_tool_header,
    SEQUENCER_MT_context_menu,
    DOPESHEET_MT_preview_range,
    SEQUENCER_PT_compare,
    PREVIEW_PT_stax_tools,
    PREVIEW_PT_pending_text_note,
    PREVIEW_PT_text_notes,
    PREVIEW_PT_media_info,
)


def register():
    # Set Custom UI
    # -------------
    for cls in classes:
        register_class(cls)


def unregister():
    # Clear Custom UI
    for cls in classes:
        unregister_class(cls)
