# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

import bpy

from stax.utils.utils_timeline import get_media_sequence


class TEXT_HT_header(bpy.types.Header):
    """Text space header"""

    bl_space_type = "TEXT_EDITOR"

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        space_data = context.space_data
        text = space_data.text

        # Text edit Stax UI
        row = layout.row()
        ops = row.operator("wm.write_comment", text="Confirm", icon="TEXT")
        ops.confirm = True
        ops.parent_name = text.get("parent", "")

        row.operator(
            "wm.delete_comment", icon="CANCEL"
        ).text_obj_name = context.space_data.text.name

        layout.separator_spacer()

        # Comment range
        row = layout.row(align=True)
        row.label(text="Comment Range")

        # Button to link only to current frame
        sub = row.row()
        current_frame_only = (
            scene.frame_preview_end - scene.frame_preview_start == 0
            and scene.frame_current == scene.frame_preview_start
        )
        sub.operator(
            "sequencer.set_range_current_frame",
            text=f"Set Range to Current Frame ({scene.media_current_frame})",
            depress=current_frame_only,
        )

        # Preview Range values
        sub = row.row(align=True)
        sub.prop(scene, "frame_preview_start", text="Start")
        sub.prop(scene, "frame_preview_end", text="End")
        sub.prop(scene, "use_preview_range", icon="CANCEL", text="")

        layout.separator_spacer()

        # Original Blender UI
        if context.scene.user_preferences.show_blender_ui:

            if text and text.is_modified:
                row = layout.row(align=True)
                row.alert = True
                row.operator("text.resolve_conflict", text="", icon="HELP")

            layout.separator_spacer()

            row = layout.row(align=True)
            row.template_ID(
                space_data,
                "text",
                new="text.new",
                unlink="text.unlink",
                open="text.open",
            )

            layout.separator_spacer()

            row = layout.row(align=True)
            row.prop(space_data, "show_line_numbers", text="")
            row.prop(space_data, "show_word_wrap", text="")

            is_syntax_highlight_supported = space_data.is_syntax_highlight_supported()
            syntax = row.row(align=True)
            syntax.active = is_syntax_highlight_supported
            syntax.prop(space_data, "show_syntax_highlight", text="")

            if text and "REPLY" not in text.name:
                row = layout.row()
                if text.name.endswith((".osl", ".oso")):
                    row.operator("node.shader_script_update")

                else:
                    row.active = text.name.endswith(".py")
                    row.prop(text, "use_module")

                    row = layout.row()
                    row.active = is_syntax_highlight_supported
                    row.operator("text.run_script")


classes = [TEXT_HT_header]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
