import importlib
from pathlib import Path

import bpy

import openreviewio as orio


def test_stax_import():
    """Stax module import"""

    assert importlib.import_module("bl_app_templates_user.stax")


def test_openreviewio_import():
    """OpenReviewIO module import"""

    assert importlib.import_module("openreviewio")


def test_link_reviews():
    """Link reviews"""
    # Load test timeline
    bpy.ops.wm.open_mainfile(
        filepath=Path(__file__)
        .parent.joinpath("data", "timeline_test.blend")
        .as_posix()
    )

    orio_dir = Path(__file__).parent.joinpath("data", "OpenReviewIO")
    scene = bpy.context.scene
    sequence_editor = scene.sequence_editor

    # Link reviews
    bpy.ops.sequencer.select_all(action="SELECT")
    bpy.ops.sequencer.link_sequences_reviews(directory=str(orio_dir))

    # Test reviews linked to sequences
    for seq in sequence_editor.sequences:
        # The review exists
        review_path = seq.get("orio_review_path")
        if review_path:
            assert review_path.is_dir()

            # It's the same as the get_orio_review function
            orio_review = orio.load_media_review(review_path)
            assert orio_review
